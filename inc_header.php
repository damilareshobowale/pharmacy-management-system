<?php

$currentuserid = getCookie("userid");
//get the details of the cart
$cartdetails = $dataRead->cart_getbyidmember($mycon, $currentuserid, '5');

//get the list of the order that has either been confirmed, not unconfirmed

?>

<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li style="margin-left: 15px;">
                        <form role="search" class="app-search hidden-xs" action="product_search.php" method="get">
                            <input type="text" placeholder="Search for products..." value="<?php if (isset($_GET['s']) && $_GET['s'] != '') echo $_GET['s']; ?>" name="s" class="form-control" style="width: 300px">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                    <li>
                        <a class="waves-effect waves-light" href="index.php"><span class="hidden-xs">Home </span> <i class="fa fa-home"></i></a>
                        </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Product Categories </span> <i class="fa fa-sort-down"></i></a>
                        <ul class="dropdown-menu dropdown-cart dropdown-tasks animated slideInUp">
                            <?php

                                foreach ($categorydetails as $row ) {
                                
                            ?>
                            <li>
                                <div class="cart-img"><img src="dashboard/uploads/category/<?php echo $row['category_id'] ?>.jpg" /></div>
                                <div class="cart-content">
                                    <a href="javascript:void(0);"><h5><?php echo ucfirst(strtolower($row['name'])); ?></h5><small>>></small></a></div>
                            </li>
                            <li class="divider"></li>
                            <?php
                                }
                            ?>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa fa-bell"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="dashboard/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5>
                                            <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="dashboard/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5>
                                            <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="dashboard/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5>
                                            <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="dashboard/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5>
                                            <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa fa-cart-plus"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu dropdown-cart dropdown-tasks animated slideInUp" id="cartappend">
                            <?php

                                foreach ($cartdetails as $row) {
                                    
                            ?>
                            <li>
                                <div class="cart-img"><img src="dashboard/uploads/product/<?php echo $row['product_id'] ?>.jpg" /></div>
                                <div class="cart-content">
                                    <h5><?php echo $row['name']." X ".$row['quantity']; ?></h5><small>&#8358;<?php echo number_format($row['discountedprice'], 0, '.', ','); ?> -- (&#8358;<?php echo number_format($row['total'], 0, '.', ','); ?>)</small></div>
                            </li>
                            <li class="divider"></li>
                            <?php

                                }

                                if ($cartdetails == null)
                                {
                                ?>
                                <li id="nocart">
                                    <div class="cart-img"></div>
                                    <div class="cart-content text-center">
                                    <h5 class="text-center" style="margin: 10px;"><em>Cart Is Empty</em></h5></div>
                                </li>
                            <li class="divider"></li>
                            <?php

                                }
                                ?>
                                <li>
                                <a class="text-center" href="product_checkout.php"> <strong>Checkout</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <?php

                    if ($memberdetails)
                    {

                    ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="dashboard/plugins/images/users/d1.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $memberdetails['lastname']." ".$memberdetails['firstname'] ?></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="index.php"><i class="fa fa-home"></i> Go Home</a></li>
                            <li><a href="profile.php"><i class="ti-user"></i>  My Profile</a></li>
                            <li><a href="settings.php"><i class="ti-settings"></i>  Account Setting</a></li>
                            <li><a href="dashboard/login.php?logout=yes"><i class="fa fa-power-off"></i>  Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <?php

                        }
                        else
                        {
                    ?>
                    <li><a href="dashboard/login.php"><button class="btn btn-primary"><i class="fa fa-lock"></i> Login Here</button></a></li>
                    <?php
                        }
                        ?>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>