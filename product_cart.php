<?php
include_once("dashboard/admin/config.php");
include_once("dashboard/admin/inc_dbfunctions.php");

//generate a random id number
$random = substr(str_shuffle(time()),0,4);

$currentuserid = getCookie("userid");
$mycon = databaseConnect();

$dataRead = New DataRead();

//get the details of the member
$memberdetails = $dataRead->member_getbyid($mycon,$currentuserid);

$productdetails = $dataRead->product_getall($mycon);

//get the list of all category
$categorydetails = $dataRead->category_getall($mycon);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" src="dashboard/plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="dashboard/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link src="dashboard/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link src="dashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link src="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="dashboard/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="dashboard/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php");  ?>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">All Products</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li class="active">All Products</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                    <!-- /.row -->
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-sm-7">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Your Cart (5 items)</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table product-overview">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Product info</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th style="text-align:center">Total</th>
                                                    <th style="text-align:center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td width="150"><img src="../plugins/images/chair.jpg" alt="iMac" width="80"></td>
                                                    <td width="550">
                                                        <h5 class="font-500">Rounded Chair</h5>
                                                        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look</p>
                                                    </td>
                                                    <td>$153</td>
                                                    <td width="70">
                                                        <input type="text" class="form-control" placeholder="1">
                                                    </td>
                                                    <td width="150" align="center" class="font-500">$153</td>
                                                    <td align="center"><a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash text-dark"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../plugins/images/chair2.jpg" alt="iMac" width="80"></td>
                                                    <td>
                                                        <h5 class="font-500">Rounded Chair</h5>
                                                        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look</p>
                                                    </td>
                                                    <td>$153</td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="1">
                                                    </td>
                                                    <td class="font-500" align="center">$153</td>
                                                    <td align="center"><a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash text-dark"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../plugins/images/chair3.jpg" alt="iMac" width="80"></td>
                                                    <td>
                                                        <h5 class="font-500">Rounded Chair</h5>
                                                        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look</p>
                                                    </td>
                                                    <td>$153</td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="1">
                                                    </td>
                                                    <td class="font-500" align="center">$153</td>
                                                    <td align="center"><a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash text-dark"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td><img src="../plugins/images/chair4.jpg" alt="iMac" width="80"></td>
                                                    <td>
                                                        <h5 class="font-500">Rounded Chair</h5>
                                                        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look</p>
                                                    </td>
                                                    <td>$153</td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="1">
                                                    </td>
                                                    <td class="font-500" align="center">$153</td>
                                                    <td align="center"><a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash text-dark"></i></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        <button class="btn btn-danger pull-right"><i class="fa fa fa-shopping-cart"></i> Checkout</button>
                                        <button class="btn btn-default btn-outline"><i class="fa fa-arrow-left"></i> Continue shopping</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="white-box">
                                    <h3 class="box-title">Cart Summary</h3>
                                    <hr> <small>Total Price</small>
                                    <h2>$612</h2>
                                    <hr>
                                    <button class="btn btn-success">Checkout</button>
                                    <button class="btn btn-default btn-outline">Cancel</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="white-box">
                                    <h3 class="box-title">For Any Support</h3>
                                    <hr>
                                    <h4><i class="ti-mobile"></i> 9998979695 (Toll Free)</h4> <small>Please contact with us if you have any questions. We are avalible 24h.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"><?php echo date("Y"); ?> &copy; Web Based Pharmacy Management System </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Counter js -->
    <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="js/dashboard1.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
