<?php

include_once("dashboard/admin/inc_dbfunctions.php");
include_once("dashboard/admin/config.php");

//generate a random id number
$random = substr(str_shuffle(time()),0,4);

$currentuserid = '';
if (getCookie("userid") != '')
{
    $currentuserid = getCookie("userid");
}
$mycon = databaseConnect();

$dataRead = New DataRead();

//get the details of the member
$memberdetails = $dataRead->member_getbyid($mycon,$currentuserid);


$productdetails = $dataRead->product_getall($mycon);

//get the list of all category
$categorydetails = $dataRead->category_getall($mycon);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="dashboard/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dashboard/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="dashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="dashboard/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="dashboard/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php");  ?>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">All Products</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li class="active">All Products</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div id="result"></div>   
                    <?php

                        foreach ($productdetails as $row) {
                            $image = getimagesize("dashboard/uploads/product/".$row['product_id'].".jpg");
                            
                        
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="product-img">
                                <img src="dashboard/uploads/product/<?php echo $row['product_id'] ?>.jpg" <?php echo imageResize("dashboard/uploads/product/".$row['product_id'].".jpg", 249.11, 200) ?> />
                                <div class="pro-img-overlay"><a href="product_view.php?product=<?php echo $row['product_id'] ?>" class="bg-info"><i class="fa fa-eye"></i></a></div>
                            </div>
                            <div class="product-text">
                                <span class="pro-price bg-info">&#8358;<?php echo number_format($row['discountedprice'], 0, '.', ','); ?></span>
                                <h3 class="box-title m-b-0"><?php echo $row['name'] ?></h3>
                                <small class="text-muted db">Category: 
                                    <?php
                                        $Category_get = $dataRead->category_getbyidproduct($mycon, $row['product_id']);
                                            foreach($Category_get as $key)
                                            {
                                                echo "<a href='category_add.php?id=".$key['category_id']."'><span>".strtolower($key['name'])."</span></a> - ";
                                            }
                                    ?>
                                </small>
                            </div>
                        </div>
                    </div>
                     <?php

                        }

                        ?>
                </div>
                <!--row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"><?php echo date("Y"); ?> &copy; Web Based Pharmacy Management System </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="dashboard/bootstrap/dist/js/tether.min.js"></script>
    <script src="dashboard/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="dashboard/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="dashboard/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="dashboard/js/waves.js"></script>
    <!--Counter js -->
    <script src="dashboard/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="dashboard/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="dashboard/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="dashboard/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dashboard/js/custom.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="dashboard/js/dashboard1.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="dashboard/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script type="text/javascript">
        
        function addToCart(product_id, stock)
        {
            quantity = 1;
            if (quantity == 0)
            {
                alert("Quantity can not be zero, minimum is 1");
                return;
             /* Send the data using post */
            }
            if (quantity > stock)
            {
                alert("Sorry, the quantity you want to purchase is more than the available in stock");
                return;
            }
            var posting = $.post('dashboard/admin/actionmanager.php', {
                product_id: product_id,
                stock: parseInt(stock),
                quantity: quantity,
                command: "cart_add" 
            });
            
            /* Put the results in a div */
            posting.done(function(data) {
                $("#result").html(data);  
            });
        }
    </script>
</body>

</html>
