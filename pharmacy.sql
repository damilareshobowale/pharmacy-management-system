-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2018 at 08:02 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `id` varchar(500) NOT NULL,
  `name` varchar(500) NOT NULL,
  `info` text NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `id`, `name`, `info`, `createdby`, `createdon`, `updatedon`) VALUES
(7, 'PCID1996', 'cough', 'this is cough', 5, '2018-01-16 16:52:48', '0000-00-00 00:00:00'),
(9, 'PCID7511', 'stomach ache', 'this is stomach ache', 5, '2018-01-16 17:06:40', '0000-00-00 00:00:00'),
(13, 'PCID1330', 'tuberculosis', 'this is tuberculosis', 5, '2018-01-16 21:05:21', '2018-01-16 21:10:16'),
(14, 'PCID3516', 'diseases', 'this is the deases', 5, '2018-01-17 06:02:38', '2018-01-18 21:16:27'),
(17, 'PCID2661', 'challenge', 'this is a challenge', 5, '2018-01-17 06:15:42', '2018-01-18 21:16:57'),
(22, 'PCID2611', 'sesdh', 'fsjdfsjsd', 5, '2018-01-17 06:33:53', '2018-01-18 21:17:44'),
(27, 'PCID2106', 'hello', 'gffch', 5, '2018-01-17 06:48:30', '2018-01-18 21:18:38'),
(31, 'PCID1332', 'hsdghshgfsdsdgh', 'hfdhfbhfbsfbs', 5, '2018-01-18 01:27:13', '2018-01-18 21:19:07'),
(32, 'PCID2166', 'sdfsds', 'dfdfd', 5, '2018-01-18 12:59:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `id` varchar(100) NOT NULL,
  `name` varchar(500) NOT NULL,
  `info` text NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `id`, `name`, `info`, `createdon`, `updatedon`) VALUES
(3, 'DID5861', 'Sales Department', 'This is the sales department. here they handles anything regarding the selling and buying', '2018-01-16 07:35:25', '2018-01-16 07:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `member_id` int(11) NOT NULL,
  `id` varchar(100) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `email` varchar(800) NOT NULL,
  `password` text NOT NULL,
  `phonenumber` varchar(500) NOT NULL,
  `department_id` int(11) NOT NULL,
  `position` varchar(500) NOT NULL,
  `age` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `currentsalary` varchar(500) NOT NULL,
  `info` text NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`member_id`, `id`, `firstname`, `lastname`, `email`, `password`, `phonenumber`, `department_id`, `position`, `age`, `startdate`, `currentsalary`, `info`, `createdon`, `updatedon`, `type`, `status`) VALUES
(5, 'SID7680', 'Damilare', 'Shobowale', 'oluwadamilareshobowale@gmail.com', '27bfaa8f76fe30db4d3d2fec408d0b29a772bdd3bf5ee330951233473d94d0e9', '08136368738', 3, 'Sales Manager', 34, '2018-01-09', '50000', '', '2018-01-16 14:21:55', '0000-00-00 00:00:00', 3, 5),
(6, 'CUID1132', 'ndasgdvag', 'jsdbvsdsd', 'gbolahan@gmail.com', '27bfaa8f76fe30db4d3d2fec408d0b29a772bdd3bf5ee330951233473d94d0e9', '08136368739', 0, '', 0, '0000-00-00', '', 'thshdbshs', '2018-01-18 02:10:01', '0000-00-00 00:00:00', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `id` varchar(500) NOT NULL,
  `name` varchar(500) NOT NULL,
  `info` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `price` varchar(500) NOT NULL,
  `discountedprice` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `id`, `name`, `info`, `member_id`, `stock`, `createdon`, `updatedon`, `status`, `price`, `discountedprice`) VALUES
(7, 'PID3535', 'Athemeter', 'this is a malaria drug', 5, 38, '2018-01-18 01:35:48', '0000-00-00 00:00:00', 3, '500', '450'),
(8, 'PID5156', 'faasas', 'aasasaghs', 5, 60, '2018-01-18 01:38:08', '0000-00-00 00:00:00', 5, '1200', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `productcart`
--

CREATE TABLE `productcart` (
  `productcart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `quantity` varchar(500) NOT NULL,
  `total` varchar(500) NOT NULL,
  `createdon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productcart`
--

INSERT INTO `productcart` (`productcart_id`, `product_id`, `member_id`, `quantity`, `total`, `createdon`) VALUES
(4, 7, 6, '113', '167400', '2018-01-20 14:16:49'),
(5, 6, 6, '20', '9000', '2018-01-20 14:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `productcategory_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `createdon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`productcategory_id`, `product_id`, `category_id`, `member_id`, `createdon`) VALUES
(1, 3, 7, 5, '2018-01-17 16:27:05'),
(2, 3, 9, 5, '2018-01-17 16:27:05'),
(3, 4, 7, 5, '2018-01-17 16:29:36'),
(4, 4, 9, 5, '2018-01-17 16:29:36'),
(5, 4, 14, 5, '2018-01-17 16:29:36'),
(6, 5, 7, 5, '2018-01-17 22:24:34'),
(7, 5, 9, 5, '2018-01-17 22:24:34'),
(8, 5, 13, 5, '2018-01-17 22:24:34'),
(9, 5, 22, 5, '2018-01-17 22:24:34'),
(40, 7, 7, 5, '2018-01-18 01:35:48'),
(41, 7, 9, 5, '2018-01-18 01:35:48'),
(42, 7, 13, 5, '2018-01-18 01:35:48'),
(43, 7, 14, 5, '2018-01-18 01:35:48'),
(44, 8, 7, 5, '2018-01-18 01:38:08'),
(45, 8, 9, 5, '2018-01-18 01:38:08'),
(51, 6, 7, 5, '2018-01-19 00:00:22'),
(52, 6, 9, 5, '2018-01-19 00:00:22'),
(53, 6, 13, 5, '2018-01-19 00:00:22'),
(54, 6, 14, 5, '2018-01-19 00:00:22'),
(55, 6, 31, 5, '2018-01-19 00:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `producttag`
--

CREATE TABLE `producttag` (
  `producttag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `createdon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttag`
--

INSERT INTO `producttag` (`producttag_id`, `product_id`, `tag_id`, `member_id`, `createdon`) VALUES
(1, 3, 1, 5, '2018-01-17 16:27:05'),
(2, 3, 3, 5, '2018-01-17 16:27:05'),
(3, 4, 1, 5, '2018-01-17 16:29:36'),
(4, 5, 5, 5, '2018-01-17 22:24:34'),
(5, 5, 6, 5, '2018-01-17 22:24:34'),
(20, 7, 3, 5, '2018-01-18 01:35:48'),
(21, 7, 4, 5, '2018-01-18 01:35:48'),
(22, 8, 3, 5, '2018-01-18 01:38:08'),
(23, 8, 4, 5, '2018-01-18 01:38:08'),
(25, 6, 5, 5, '2018-01-19 00:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL,
  `id` varchar(500) NOT NULL,
  `name` varchar(500) NOT NULL,
  `info` text NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tag_id`, `id`, `name`, `info`, `createdby`, `createdon`, `updatedon`) VALUES
(1, 'PTID5356', 'health', 'This is the health', 5, '2018-01-16 21:47:32', '2018-01-16 21:59:30'),
(3, 'PTID1366', 'xcxcxc', 'zsdsdsd', 5, '2018-01-17 06:52:41', '0000-00-00 00:00:00'),
(4, 'PTID1211', 'ererer`', 'wesewe', 5, '2018-01-17 06:57:12', '0000-00-00 00:00:00'),
(5, 'PTID1235', 'xdsds', 'fdfdfd', 5, '2018-01-17 22:16:12', '0000-00-00 00:00:00'),
(6, 'PTID1024', 'cfxfxf', 'gcfcghjmll', 5, '2018-01-17 22:21:29', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `productcart`
--
ALTER TABLE `productcart`
  ADD PRIMARY KEY (`productcart_id`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`productcategory_id`);

--
-- Indexes for table `producttag`
--
ALTER TABLE `producttag`
  ADD PRIMARY KEY (`producttag_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `productcart`
--
ALTER TABLE `productcart`
  MODIFY `productcart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `productcategory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `producttag`
--
ALTER TABLE `producttag`
  MODIFY `producttag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
