<?php
include_once("dashboard/admin/config.php");
include_once("dashboard/admin/inc_dbfunctions.php");

//generate a random id number
$random = substr(str_shuffle(time()),0,4);

$currentuserid = getCookie("userid");
$mycon = databaseConnect();

$dataRead = New DataRead();

//get the details of the member
$memberdetails = $dataRead->member_getbyid($mycon,$currentuserid);


//get the list of all category
$categorydetails = $dataRead->category_getall($mycon);
if (isset($_GET['product']) && $_GET['product'] != '') $id = $_GET['product']; 
else
{
    showAlert("No product selected. Please select a product to view");
    openPage("index.php");
}
//get the id of the department
$productdetails = $dataRead->product_getbyid($mycon, $id);
if (!$productdetails)
{
    showAlert("Product not found. Please select a product to view");
    openPage("index.php");
}

$relatedproduct = $dataRead->product_getall($mycon);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="dashboard/plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="dashboard/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dashboard/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="dashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="dashboard/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="dashboard/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php");  ?>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">All Products</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">All products</a></li>
                            <li class="active">View Products</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                    <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0"><?php echo ucwords(strtolower($productdetails['name'])) ?></h2> <small class="text-muted db"> Category: 
                                    <?php
                                        $Category_get = $dataRead->category_getbyidproduct($mycon, $productdetails['product_id']);
                                            foreach($Category_get as $key)
                                            {
                                                echo "<a href='category_add.php?id=".$key['category_id']."'><span>".strtolower($key['name'])."</span></a> - ";
                                            }
                                    ?>
                                </small>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                        <div class="white-box text-center"> <img src="dashboard/uploads/product/<?php echo $productdetails['product_id'] ?>.jpg" class="img-responsive" /> </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <div id="result"></div>
                                        <h4 class="box-title m-t-40">Product description</h4>
                                        <p><?php echo ucfirst(strtolower($productdetails['info'])) ?></p>
                                        <h2 class="m-t-40 text-success">&#8358;<?php echo number_format($productdetails['discountedprice'], 0, '.', ',');  ?> <small class="text-success">
                                        <?php
                                            $price = $productdetails['price'];
                                            $discountedprice = $productdetails['discountedprice'];

                                            $discount = ((($price - $discountedprice) / $price) * 100);

                                            echo "(".round($discount)."% off, <span class='text-danger' style='text-decoration: line-through'>&#8358;".number_format($price, 0, '.', ',')."</span>)";
                                        ?></small></h2>
                                        <?php if ($productdetails['stock'] != 0)
                                            {
                                        ?>
                                        <input type="number" class="form-control" id="quantity" value="1" placeholder="1" style="width: 100px; margin: 30px;">
                                        <input type="hidden" class="form-control" id="stock" value="<?php echo $productdetails['stock'];  ?>" style="width: 100px; margin: 30px;">
                                        <button class="btn btn-inverse btn-rounded m-r-5" onclick="addToCart(<?php echo $productdetails['product_id']; ?>);" data-toggle="tooltip" title="Add to cart"><i class="fa fa-cart-plus"></i> </button>
                                        <h2 class="box-title m-t-40 text-success">In Stock, <?php echo $productdetails['stock'] ?> quantity </h3>
                                        <?php

                                            }
                                            else 
                                            {
                                                echo "<h2 class='box-title m-t-40 text-danger'>Not In Stock</h3>";
                                            }
                                            ?>
                                        <h3 class="box-title m-t-40">Product Category</h3>
                                        <ul class="list-icons">
                                            <?php
                                            $Category_get = $dataRead->category_getbyidproduct($mycon, $productdetails['product_id']);
                                            foreach($Category_get as $key)
                                            {
                                            ?>
                                            <li><a href="category_find.php?id<?php echo $key['category_id']; ?>"><i class="fa fa-check text-success"></i> <?php echo ucfirst(strtolower($key['name'])) ?></a></li>
                                             <?php
                                                }

                                                ?>
                                        </ul>
                                        <p class="box-title m-t-40">Product Tags: &nbsp;<?php
                                            $tag_get = $dataRead->tag_getbyidproduct($mycon, $productdetails['product_id']);
                                            foreach($tag_get as $key)
                                            {
                                            ?><a href="category.php?id<?php echo $key['tag_id']; ?>"><i class="fa fa-hashtag text-success"></i><?php echo strtolower($key['name']) ?>  &nbsp; &nbsp;</a>
                                             <?php
                                                }

                                                ?>
                                        </p>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="box-title m-t-40">General Info</h3>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="390">Product Name</td>
                                                        <td> <?php echo ucwords(strtolower($productdetails['name'])) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description</td>
                                                        <td> <?php echo ucfirst(strtolower($productdetails['name'])) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Stock</td>
                                                        <td> <?php if ($productdetails['stock'] != 0) echo "Yes"; else echo "No"; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quantity In Stock</td>
                                                        <td> <?php echo $productdetails['stock']; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Category</td>
                                                        <td> <?php
                                                            $Category_get = $dataRead->category_getbyidproduct($mycon, $productdetails['product_id']);
                                                                foreach($Category_get as $key)
                                                                {
                                                                    echo "<a href='category.php?id=".$key['category_id']."'><span>".strtolower($key['name'])."</span></a> - ";
                                                                }
                                                        ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tags</td>
                                                        <td><?php
                                                            $tag_get = $dataRead->tag_getbyidproduct($mycon, $productdetails['product_id']);
                                                                foreach($tag_get as $key)
                                                                {
                                                                    echo "<a href='tags.php?id=".$key['tag_id']."'><span>".strtolower($key['name'])."</span></a> - ";
                                                                }
                                                        ?>  
                                                         </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Discounted Price</td>
                                                        <td>&#8358;<?php echo number_format($discountedprice, 0, '.', ','); ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Price</td>
                                                        <td>&#8358;<?php echo number_format($price, 0, '.', ','); ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Discount Off (%)</td>
                                                        <td><?php echo round($discount)."%"; ?> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->
                 <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <h2 class="m-b-0 m-t-0">Related Products</h2>
                    </div>
                    <hr>
                    <br />
                    <?php

                        foreach ($relatedproduct as $row) {
                            $image = getimagesize("dashboard/uploads/product/".$row['product_id'].".jpg");
                            
                        
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="product-img">
                                <img src="dashboard/uploads/product/<?php echo $row['product_id'] ?>.jpg" <?php echo imageResize("dashboard/uploads/product/".$row['product_id'].".jpg", 249.11, 200) ?> />
                                <div class="pro-img-overlay"><a href="product_view.php?product=<?php echo $row['product_id'] ?>" class="bg-info"><i class="fa fa-eye"></i></a> <a href="javascript:void(0)" class="bg-danger"><i class="fa fa-cart-plus"></i></a></div>
                            </div>
                            <div class="product-text">
                                <span class="pro-price bg-info">&#8358;<?php echo number_format($row['discountedprice'] , 0, '.', ',')  ?></span>
                                <h3 class="box-title m-b-0"><?php echo $row['name'] ?></h3>
                                <small class="text-muted db">Category: 
                                    <?php
                                        $Category_get = $dataRead->category_getbyidproduct($mycon, $row['product_id']);
                                            foreach($Category_get as $key)
                                            {
                                                echo "<a href='category_add.php?id=".$key['category_id']."'><span>".strtolower($key['name'])."</span></a> - ";
                                            }
                                    ?>
                                </small>
                            </div>
                        </div>
                    </div>
                     <?php

                        }

                        ?>
                </div>
                <!--row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"><?php echo date("Y"); ?> &copy; Web Based Pharmacy Management System </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="dashboard/bootstrap/dist/js/tether.min.js"></script>
    <script src="dashboard/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="dashboard/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="dashboard/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="dashboard/js/waves.js"></script>
    <!--Counter js -->
    <script src="dashboard/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="dashboard/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="dashboard/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="dashboard/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dashboard/js/custom.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="dashboard/js/dashboard1.js"></script>
    <!-- Sweet-Alert  -->
    <script src="dashboard/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="dashboard/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="dashboard/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script type="text/javascript">
        
        function addToCart(product_id)
        {
            quantity = $('#quantity').val();
            stock = $('#stock').val();
            console.log(quantity);
            if (quantity == 0)
            {
                alert("Quantity can not be zero, minimum is 1");
                return;
             /* Send the data using post */
            }
            if (quantity > stock)
            {
                alert("Sorry, the quantity you want to purchase is more than the available in stock");
                return;
            }
            var posting = $.post('dashboard/admin/actionmanager.php', {
                product_id: product_id,
                stock: stock,
                quantity: quantity,
                command: "cart_add" 
            });
            
            /* Put the results in a div */
            posting.done(function(data) {
                $("#result").html(data);  
            });
        }
    </script>
</body>

</html>
