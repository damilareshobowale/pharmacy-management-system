<?php
include_once("admin/config.php");
include_once("admin/inc_dbfunctions.php");

$dataRead = New DataRead();
$mycon = databaseConnect();


//generate a random id number
$random = substr(str_shuffle(time()),0,4);

//get the details of the staff
$staffdetails = $dataRead->member_getall($mycon);

//get the list of all category
$categorydetails = $dataRead->category_getall($mycon);
$editCat = '';
$categoryeditdetails = '';
if (isset($_GET['id']) && $_GET['id'] != '') 
    {
        $editCat = $_GET['id'];
        $categoryeditdetails = editCategory($editCat); 

    }

function editCategory($id)
{
    $mycon = databaseConnect();
    $dataRead = New DataRead();

    $categoryedit = $dataRead->category_getbyidcategory($mycon, $id);
    if (!$categoryedit)
    {
        showAlert("Category not found. Please select a category to edit");
        openPage("category_add.php");
    }

    return $categoryedit;
}

if (isset($_GET['delete']) && $_GET['delete'] != '') deleteCategory($_GET['delete']);


function deleteCategory($delete)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //find if the id exists
    $category_check = $dataRead->category_getbyidcategory($mycon,$delete);
    if (!$category_check)
    {
        showAlert("Sorry, this category no longer exists.");
        openPage("category_add.php");
    }

    //delete the category and then refresh the page
    $category_delete = $dataWrite->category_delete($mycon, $delete);
    if (!$category_delete)
    {
        showAlert("Category could not be deleted. Please try again");
        openPage("category_add.php");
    }

    showAlert("Category has been deleted. Please press OK to refresh");
    openPage("category_add.php");
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php"); ?>

        <?php include_once("inc_sidebar.php"); ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Product Category</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard.php">Pharmacy</a></li>
                            <li class="active">Product Category</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-5 col-md-5">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Add New Product Category</h3>
                            <p class="text-muted m-b-30 font-13"> Add new product category to the pharmacy <button class="btn btn-info btn-rounded" type="button" onclick="location.href='category_add.php'">Add New </button></p>
                            <form class="form" action="admin/actionmanager.php" id="<?php if ($editCat != '') echo "category_update"; else echo "category_add" ?>" method="post" enctype="multipart/form-data">
                                <div id="result"></div>
                                <div class="form-group row">
                                    <label for="category_id" class="col-2 col-form-label">Category ID:</label>
                                    <div class="col-10">
                                        <input class="form-control" name="category_id" type="text" value="<?php if ($editCat != '') echo $categoryeditdetails['id']; else echo "PCID".$random; ?>" id="category_id" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-2 col-form-label">Category Name</label>
                                    <div class="col-10">
                                        <input class="form-control" type="text" id="name" name="name" value="<?php if ($editCat != '') echo $categoryeditdetails['name'] ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="info" class="col-2 col-form-label">Desc</label>
                                    <div class="col-10">
                                        <textarea class="form-control" name="info" id="info"><?php if ($editCat != '') echo $categoryeditdetails['info'] ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="image" class="col-2 col-form-label">Upload Image</label>
                                    <div class="col-10">
                                        <input class="form-control" type="file" id="image" name="image">
                                    </div>
                                </div>
                                <?php if ($editCat != '')
                                {
                                    ?>

                                    <div class="form-group row">
                                    <label class="col-sm-12">Category Image</label>
                                    <div class="col-sm-12">
                                        <img class="img-responsive" src="Uploads/category/<?php echo $categoryeditdetails['category_id'] ?>.jpg" alt="<?php echo $categoryeditdetails['name'] ?>" style="max-width:120px;">
                                    </div>
                                </div>
                                <?php

                                }
                                ?>
                                 <div class="form-group text-center">
                                    <button class="btn btn-info btn-rounded" type="submit" id="<?php if ($editCat != '') echo "category_updatebutton"; else echo "category_addbutton" ?>" >Save</button>
                                    <button class="btn btn-default btn-rounded" type="reset">Reset</button>    
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-7">
                         <div class="white-box">
                            <h3 class="box-title m-b-0">Product Category</h3>
                            <p class="text-muted m-b-30">List of all category for products</p>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID:</th>
                                            <th>Name</th>
                                            <th> Created By </th> 
                                            <th style="width: 20%">Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID:</th>
                                            <th>Name</th>#
                                            <th> Created By </th> 
                                            <th style="width: 20%">Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                            $count = 0;
                                            foreach ($categorydetails as $row) {
                                            $id = $row['category_id'];
                                        ?>
                                        <tr>
                                            <td><?php echo $row['id']; ?></td>
                                            <td><?php echo $row['name']; ?></td>
                                            <td><?php echo $row['lastname']. " ".$row['firstname'] ?></td>
                                            <td><?php echo $row['info']; ?></td>
                                            <td><a href="category_add.php?id=<?php echo $row['category_id'] ?>" ><button class="btn btn-info btn-xs" type="button">Edit</button></a> <button class="btn btn-danger btn-xs" type="button" onclick='if (confirm("Are you sure you want to delete this category?")) location.href="category_add.php?delete=<?php echo $id ?>"'>Delete</button></td>
                                        </tr>
                                        <?php

                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Web Based Pharmacy Management Systemn</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Morris JavaScript -->
    <script src="plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jQuery peity -->
    <script src="plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="plugins/bower_components/peity/jquery.peity.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="js/dashboard1.js"></script>
    <!--Style Switcher -->

    <script src="js/ajax.js"></script>


    <script src="plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });
        });
    });
    </script>
</body>

</html>
