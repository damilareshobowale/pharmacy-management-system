        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="plugins/images/users/d1.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo getCookie("fullname") ?><span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="login.php?logout=yes"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    <li> <a href="dashboard.php" class="waves-effect"><span class="hide-menu">Dashboard</span></a> </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><span class="hide-menu"> Product/Inventory Management <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="product_add.php">Add New Products</a> </li>
                            <li> <a href="product_view.php">View All Products</a> </li>
                            <li> <a href="Inventory.php">Inventories</a> </li>
                            <li> <a href="shop_stocks.php">View Shop Stocks</a> </li>
                            <li> <a href="category_add.php">Categories</a> </li>
                            <li> <a href="tags.php">Tags</a> </li>
                            <li> <a href="shortages.php">Shortage</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><span class="hide-menu"> Staff Management <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="staff_add.php">Add New Staff</a> </li>
                            <li> <a href="staff_view.php">View All Staffs</a> </li>
                            <li> <a href="department_add.php">Add New Department</a> </li>
                            <li> <a href="department_view.php">View All Departments</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><span class="hide-menu">Inventory Management <span class="fa arrow"></span></span></a>
                          <ul class="nav nav-second-level">
                            <li> <a href="inventory_add.php">Add Inventory</a> </li>
                            <li> <a href="inventory_view.php">View All Inventories</a> </li>
                            <li> <a href="order_view.php">View Order</a> </li>
                            <li> <a href="order_rejected.php">View Rejected Order</a> </li>
                            <li> <a href="payment_view.php">View Payments</a> </li>
                            <li> <a href="sales_view.php">View Sales</a> </li>
                        </ul>  
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><span class="hide-menu"> Customers Management <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="customer_add.php">Add New Customers</a> </li>
                            <li> <a href="customer_view.php">View All Customers</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><span class="hide-menu"> Reports Management <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <!-- <li> <a href="report_general.php">General Reports</a> </li> -->
                            <!-- <li> <a href="report_purchases.php">Purchases Reports</a> </li> -->
                            <li> <a href="report_inventory.php">Inventory Reports</a> </li>
                            <li> <a href="report_invoice.php">Invoice Reports</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>