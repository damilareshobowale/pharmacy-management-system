<?php
include_once("admin/config.php");
include_once("admin/inc_dbfunctions.php");

//generate a random id number
$random = substr(str_shuffle(time()),0,4);

$dataRead = New DataRead();
//get the list zof all departments
$mycon = databaseConnect();
$departmentdetails = $dataRead->departments_getall($mycon);


$categorydetails = $dataRead->category_getall($mycon);

//get the list of all category
$tagdetails = $dataRead->tag_getall($mycon);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <?php include_once("inc_header.php"); ?>

        <?php include_once("inc_sidebar.php"); ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Add New Product</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard.php">Pharmacy</a></li>
                            <li class="active">Add New Product</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Add New Product</h3>
                            <p class="text-muted m-b-30 font-13"> Add new product to the pharmacy</p>
                            <form class="form" action="admin/actionmanager.php" id="product_add" method="post">
                                <div id="result"></div>
                                <div class="form-group row">
                                    <label for="product_id" class="col-2 col-form-label">Product ID:</label>
                                    <div class="col-10" id="product_iddiv">
                                        <input class="form-control" name="product_id" type="text" value="PID<?php echo $random; ?>" id="product_id" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-2 col-form-label">Title</label>
                                    <div class="col-10 error" id="namediv">
                                        <input class="form-control" type="text" id="name" name="name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="info" class="col-2 col-form-label">Product Description</label>
                                    <div class="col-10 error" id="lastnamediv">
                                        <textarea class="form-control" id="info" name="info" rows="7"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="price" class="col-2 col-form-label">Price</label>
                                    <div class="col-10 error" id="pricediv">
                                        <input class="form-control" type="number" id="price" name="price">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="discountedprice" class="col-2 col-form-label">Discount Price</label>
                                    <div class="col-10 error" id="discountedpricediv">
                                        <input class="form-control" type="number" id="discountedprice" name="discountedprice">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="stock" class="col-2 col-form-label">In-Stock</label>
                                    <div class="col-10 error" id="stockdiv">
                                        <select name="stock" class="form-control" id="stock">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>    
                                    </div>
                                </div>
                                <div class="form-group row" id="quantity_show">
                                    <label for="quantity" class="col-2 col-form-label">Quantity in stock</label>
                                    <div class="col-10 error" id="quantitydiv">
                                        <input class="form-control" type="number" id="quantity" name="quantity">
                                    </div>
                                </div>
                                <div class="form-check row">
                                    <label for="category" class="col-2 col-form-label">Select Category For Product</label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="category[]" id="category" class="custom-control-input" 
                                        value="0">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Uncategorized</span>
                                    </label>
                                    <?php

                                        foreach ($categorydetails as $row) {
               
                                    ?>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="category[]" id="category" class="custom-control-input" value="<?php echo $row['category_id'] ?>">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $row['name'] ?></span>
                                    </label>
                                    <?php
                                        }
                                    ?>
                                    <span id="appendcategory"></span>
                                    <a href="javascript:void(0);"><span class="text-danger" id="addnew_category"><i class="fa fa-plus"></i> Add New Category </span></a>
                                </div>
                                <div id="show_category" style="display: none">
                                    <hr>
                                    <div id="cresult"></div>
                                    <div class="form-group row">
                                        <label for="category_id" class="col-2 col-form-label">Category ID:</label>
                                        <div class="col-10" id="category_iddiv">
                                            <input class="form-control" name="category_id" type="text" value="PCID<?php echo $random; ?>" id="category_id" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                    <label for="cname" class="col-2 col-form-label">Name</label>
                                    <div class="col-10 error" id="cnamediv">
                                        <input class="form-control" type="text" id="cname" name="cname">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="desc" class="col-2 col-form-label">Desc</label>
                                    <div class="col-10 error" id="descdiv">
                                        <textarea class="form-control" id="desc" name="desc"></textarea>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-info btn-rounded" type="button" id="pcategory_addbutton">Save</button>   
                                </div>
                                <a href="javascript:void(0);"><span class="text- text-danger" id="remove_category"><i class="fa fa-times"></i> Remove Category </span></a>
                                <hr>
                                </div>

                                <div class="form-check row">
                                    <label for="tag" class="col-2 col-form-label">Select Tags For Product</label>
                                    <?php

                                        foreach ($tagdetails as $row) {
               
                                    ?>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="tag[]" id="tag" class="custom-control-input" value="<?php echo $row['tag_id'] ?>">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $row['name'] ?></span>
                                    </label>
                                    <?php
                                        }
                                    ?>
                                    <span id="appendtag"></span>
                                    <a href="javascript:void(0);"><span class="text-danger" id="addnew_tag"><i class="fa fa-plus"></i> Add New Tags </span></a>
                                </div>
                                <div id="show_tag" style="display: none">
                                    <hr>
                                    <div id="tresult"></div>
                                    <div class="form-group row">
                                        <label for="tag_id" class="col-2 col-form-label">Tag ID:</label>
                                        <div class="col-10" id="tag_iddiv">
                                            <input class="form-control" name="tag_id" type="text" value="PTID<?php echo $random; ?>" id="tag_id" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                    <label for="tname" class="col-2 col-form-label">Name</label>
                                    <div class="col-10 error" id="tnamediv">
                                        <input class="form-control" type="text" id="tname" name="tname">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tdesc" class="col-2 col-form-label">Desc</label>
                                    <div class="col-10 error" id="tdescdiv">
                                        <textarea class="form-control" id="tdesc" name="tdesc"></textarea>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-info btn-rounded" type="button" id="ptag_addbutton">Save</button>   
                                </div>
                                <a href="javascript:void(0);"><span class="text- text-danger" id="remove_tag"><i class="fa fa-times"></i> Remove Tag </span></a>
                                <hr>
                                </div>

                                <div class="form-group row">
                                    <label for="image" class="col-2 col-form-label">Attach Product Image</label>
                                    <div class="col-10 error" id="imagediv">
                                        <input class="form-control" type="file" id="image" name="image">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="discountprice" class="col-2 col-form-label">Status</label>
                                    <div class="col-10 error" id="discountpricediv">
                                        <select name="status" class="form-control" id="status">
                                            <option value="5">Post Imediately</option>
                                            <option value="3">Save as Draft</option>
                                        </select>    
                                    </div>
                                </div>
                                 <div class="form-group text-center">
                                    <button class="btn btn-info btn-rounded" type="submit" id="product_addbutton">Submit</button>
                                    <button class="btn btn-default btn-rounded" type="reset">Reset</button>    
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Web Based Pharmacy Management Systemn</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Morris JavaScript -->
    <script src="plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jQuery peity -->
    <script src="plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="plugins/bower_components/peity/jquery.peity.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="js/dashboard1.js"></script>
    <!--Style Switcher -->

    <script src="js/ajax.js"></script>
    <script type="text/javascript">
        $('#addnew_category').on('click', function(event)
        {
            $(this).hide();
            $('#show_category').show(500);
            $('#show_tag').hide(500);
        });
        $('#remove_category').on('click', function(event)
        {
            $('#cname').val('');
            $('#des').val('');
            $('#show_category').hide(500);
            $('#addnew_category').show(500);
            $('#show_tag').hide(500);
        });
        $('#addnew_tag').on('click', function(event)
        {
            $(this).hide();
            $('#show_tag').show(500);
            $('#show_category').hide(500);
        });
        $('#remove_tag').on('click', function(event)
        {
            $('#cname').val('');
            $('#des').val('');
            $('#show_tag').hide(500);
            $('#addnew_tag').show(500);
            $('#show_category').hide(500);
        });

        $('#stock').on('change', function(event)
        {
            stock = $(this).val();
            
            if (stock == 'no')
            {
                $('#quantity').val('0');
                $('#quantity').attr('readonly', true);
            }
            else
            {
                $('#quantity').val('1');
                $('#quantity').attr('readonly', false);
            }
        });
    </script>
</body>

</html>
