<?php
include_once("admin/config.php");
include_once("admin/inc_dbfunctions.php");


$dataRead = New DataRead();
//get the list zof all departments
$mycon = databaseConnect();
$productdetails = $dataRead->inventory_getall($mycon);

if (isset($_GET['delete']) && $_GET['delete'] != '') deleteInventory($_GET['delete']);


function deleteInventory($delete)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();
    //find if the id exists
    $product_check = $dataRead->inventory_getbyid($mycon,$delete);
    if (!$product_check)
    {
        showAlert("Sorry, this inventory no longer exists.");
        openPage("inventory_view.php");
    }

    //delete the department and then refresh the page
    $inventory_delete = $dataWrite->inventory_delete($mycon, $delete);
    if ($inventory_delete)
    {
        showAlert("Inventory successfully deleted. Thanks. Press OK to refresh page");
        openPage("inventory_view.php");
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php"); ?>

        <?php include_once("inc_sidebar.php"); ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">View All Inventory</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard.php">Pharmacy</a></li>
                            <li class="active">View All Inventory</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Pharmacy Inventory</h3>
                            <p class="text-muted m-b-30">List of all the inventory in the pharmacy</p>
                            <button class="btn btn-success btn-md" type="button" onclick="if (confirm('Are you sure you want to printt the report for  this inventory?')) location.href='report_inventory.php'">Print Inventory Report</button></td>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID:</th>
                                            <th style='width: 30%'>Name</th>
                                            <th>Photo</th>
                                            <th>Price </th>
                                            <th>In Stock</th>
                                            <th>Department</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID:</th>
                                            <th style='width: 30%'>Name</th>
                                            <th>Photo</th>
                                            <th>Price</th>
                                            <th>In Stock</th>
                                            <th>Department</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php

                                        $count = 0;
                                        foreach ($productdetails as $row) {
                                            $id = $row['inventory_id'];
                                        ?>
                                        <tr>
                                            <td><?php echo $row['id']; ?></td>
                                            <td><?php echo $row['name']; ?></td>
                                            <td>
                                                <img src="uploads/inventory/<?php echo $row['inventory_id'] ?>.jpg" alt="<?php echo $row['name']; ?>" width="80">
                                            </td>
                                            <td>&#8358;<?php echo $row['price']; ?></td>
                                            <td><?php 
                                                if ($row['stock'] == '0')
                                                {
                                                    echo "<span class='text-danger'> out of stock </span>";
                                                }
                                                else
                                                {
                                                    echo $row['stock'];
                                                }
                                                ?> </td>
                                            <td><?php 

                                                //get the categories for the product id
                                                $department_get = $dataRead->departments_getbyiddapartments($mycon, $row['department']);
                                                  echo "- <a href='department_view.php?id=".$department_get['name']."'><span class='text-primary'>".$department_get['name']."</span></a>";
                                
                                            ?> </td>
                                            <td><a href="inventory_edit.php?id=<?php echo $row['inventory_id'] ?>" ><button class="btn btn-info btn-xs" type="button">Edit</button></a> <button class="btn btn-danger btn-xs" type="button" onclick="if (confirm('Are you sure you want to delete this inventory?')) location.href='inventory_view.php?delete=<?php echo $id ?>'">Delete</button></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Web Based Pharmacy Management Systemn</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Morris JavaScript -->
    <script src="plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jQuery peity -->
    <script src="plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="plugins/bower_components/peity/jquery.peity.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="js/dashboard1.js"></script>
    <!--Style Switcher -->

    <script src="plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });
        });
    });
    </script>
</body>

</html>
