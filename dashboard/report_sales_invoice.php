<?php
include_once("admin/config.php");
include_once("admin/inc_dbfunctions.php");


$dataRead = New DataRead();
$mycon = databaseConnect();




if (isset($_GET['member']) && $_GET['member'] != '') $cart = $_GET['member']; 
else
{
    showAlert("No Member selected. Please select a member inorder to print invoice");
    openPage("report_invoice.php");
}

//get the details of the member first
$memberdetails = $dataRead->member_getbyid($mycon,$_GET['member']);
//get the details of the all the order made by the customers
$invoicedetails = $dataRead->productinvoice_getbyidmember($mycon, $_GET['member'], '0');
if (!$invoicedetails)
{
    showAlert("Unable to process this invoice, please try again later.");
    openPage("report_invoice.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Print Invoice</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <button class="btn btn-default" onclick="location.href='report_invoice.php'"><< Go Back</button>  
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h1 class="pull-right">INVOICE</h1>
                            <h3 class="box-title m-b-0">ABC PHARMACEUTICAL COMPANY</h3>
                            <p class="text-muted m-b-30">15, Ajewole Street, <br>Ikeja, Lagos <br>Nigeria <br>example@abc.com, +2348123412348</p>
                            <div class="pull-right">
                                <h3 class="box-title m-b-0">Invoice No: V-<?php echo $memberdetails['id'] ?></h3>
                                <p class="text-muted m-b-30">Date Generated: <?php echo formatDate(date("Y-m-d H:i:s"), "yes"); ?></p>
                            </div>
                            <div class="pull-let">
                                <h1 class="box-title m-b-0">BILLED TO: <?php echo $memberdetails['lastname']." ".$memberdetails['firstname'] ?> </h1>
                                <p class="text-muted m-b-30">Address: <?php echo $memberdetails['info'] ?> <br />
                                <?php echo $memberdetails['email'] ?>, <?php echo $memberdetails['phonenumber'] ?> </p>
                            </div>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th width="50%">Description</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $count = 0;
                                        $totalamount = 0;
                                        foreach ($invoicedetails as $row) {
                                            $totalamount = $row['total'] + $totalamount;
                                        ?>
                                        <tr>
                                            <td style="font-weight: bold"><?php echo ++$count ?></td>
                                            <td style="font-weight: bold"><?php echo $row['productid'] ?> - <?php echo $row['productname'] ?><br><?php echo $row['productinfo'] ?><br>Quantity: <?php echo $row['quantity'] ?></td>
                                            <td style="font-weight: bold">&#8358;<?php echo number_format($row['total'], 0, '.', ','); ?></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                     <tfoot>
                                        <tr>
                                            <th colspan="2">Total Amount: </th>
                                            <th>&#8358;<?php echo number_format($totalamount, 0, '.', ','); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Morris JavaScript -->
    <script src="plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jQuery peity -->
    <script src="plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="plugins/bower_components/peity/jquery.peity.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="js/dashboard1.js"></script>
    <!--Style Switcher -->

    <script src="plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function() {
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });
        });
    });
    </script>
</body>

</html>
