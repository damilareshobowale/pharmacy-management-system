<?php

require_once("inc_dbfunctions.php");
require_once("config.php");

$actionmanager = New ActionManager();

if(isset($_POST['command']) && $_POST['command'] == 'members_add')
{
    $actionmanager->members_add();
}
else if(isset($_POST['command']) && $_POST['command'] == 'token_add')
{
    $actionmanager->members_token_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "memberlogin")
{
    $actionmanager->member_login();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_add")
{
    $actionmanager->departments_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_update")
{
    $actionmanager->departments_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "staff_add")
{
    $actionmanager->staff_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "staff_update")
{
    $actionmanager->staff_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "customer_add")
{
    $actionmanager->customer_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "customer_update")
{
    $actionmanager->customer_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "category_add")
{
    $actionmanager->category_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "category_update")
{
    $actionmanager->category_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "tag_add")
{
    $actionmanager->tag_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "tag_update")
{
    $actionmanager->tag_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "pcategory_add")
{
    $actionmanager->pcategory_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "ptag_add")
{
    $actionmanager->ptag_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "product_add")
{
    $actionmanager->product_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "product_update")
{
    $actionmanager->product_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "cart_add")
{
    $actionmanager->cart_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "carddetails_add")
{
    $actionmanager->carddetails_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "inventory_add")
{
    $actionmanager->inventory_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "priority_update")
{
    $actionmanager->priority_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "students_add")
{
    $actionmanager->students_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "students_update")
{
    $actionmanager->students_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "clearance_add")
{
    $actionmanager->clearance_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "bankaccountdetails_update")
{
    $actionmanager->bankaccountdetails_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "members_update")
{
    $actionmanager->members_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "transfers_add")
{
    $actionmanager->transfers_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "receives_add")
{
    $actionmanager->receives_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "transfer_delete")
{
    $actionmanager->transfer_delete();
}
elseif(isset($_POST['command']) && $_POST['command'] == "transfer_refresh")
{
    $actionmanager->transfer_refresh();
}
elseif(isset($_POST['command']) && $_POST['command'] == "transfer_sort")
{
    $actionmanager->transfer_sort();
}
elseif(isset($_POST['command']) && $_POST['command'] == "available_balance")
{
    $actionmanager->available_balance();
}
elseif(isset($_POST['command']) && $_POST['command'] == "extendmatching")
{
    $actionmanager->extendmatching();
}
elseif(isset($_POST['command']) && $_POST['command'] == "evidence_add")
{
    $actionmanager->evidence_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "payment_confirm")
{
    $actionmanager->payment_confirm();
}
elseif(isset($_POST['command']) && $_POST['command'] == "matching_sort")
{
    $actionmanager->matching_sort();
}
elseif(isset($_POST['command']) && $_POST['command'] == "accountdetails_update")
{
    $actionmanager->accountdetails_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "memberrestore")
{
    $actionmanager->memberrestore();
}
elseif(isset($_POST['command']) && $_POST['command'] == "recovertoken_add")
{
    $actionmanager->recovertoken_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "password_update")
{
    $actionmanager->password_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "uploadfile")
{
    $actionmanager->uploadfile();
}
elseif(isset($_POST['command']) && $_POST['command'] == "unlock_account")
{
    $actionmanager->unlock_account();
}
elseif(isset($_POST['command']) && $_POST['command'] == "falsepayment")
{
    $actionmanager->falsepayment();
}
elseif(isset($_POST['command']) && $_POST['command'] == "testimony_add")
{
    $actionmanager->testimony_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "news_add")
{
    $actionmanager->news_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "news_edit")
{
    $actionmanager->news_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "testimony_add_admin")
{
    $actionmanager->testimony_add_admin();
}
elseif(isset($_POST['command']) && $_POST['command'] == "testimony_update_admin")
{
    $actionmanager->testimony_update_admin();
}


class ActionManager
{

    function members_add()
    {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $password = $_POST['password'];
        $username = $_POST['username'];
        $phonenumber = $_POST['phonenumber'];
        $email = $_POST['email'];
        $gender = $_POST['sex'];
        $country = $_POST['country'];
        $referral = $_POST['referral'];
        $address = $_POST['address'];
        $captcha = $_POST['captcha'];
        
        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        $count = 0;
        $captchaerror = '';
        $usernameerror = '';
        $emailerror = '';
        $phonenumbererror = '';
        $referralerror = '';
        $referralfinderror = '';
        
         //check if the captcha is eqaual to the session captcha
        if ($captcha != $_SESSION['captcha'])
        {
            $captchaerror = "Incorrect Captcha.";
            echo "<script type='text/javascript'>
                    $('#captchadiv').addClass('has-error');
                    </script>";
             $count = $count + 1;
        }
        //check if username exists
        $username_check = $dataRead->member_getbyusername($mycon, $username);
        if ($username_check != false)
        {
            $usernameerror = "<br>Username already exists.";
            echo "<script type='text/javascript'>
                    $('#usernamediv').addClass('has-error');
                    </script>";
             $count = $count + 1;
        }

        //check if email exists
        $email_check = $dataRead->member_getbyemail($mycon,$email);
        if ($email_check != false)
        {
            $emailerror = "<br> Email already exists.";
            echo "<script type='text/javascript'>
                    $('#emaildiv').addClass('has-error');
                    </script>";           
             $count = $count + 1;
        }

        //check if phonenumber exists
        $phonenumber_check = $dataRead->member_getbyphonenumber($mycon,$phonenumber);
        if ($phonenumber_check != false)
        {
            $phonenumbererror = "<br> Phonenumber already exists.";
            echo "<script type='text/javascript'>
                    $('#phonenumberdiv').addClass('has-error');
                    </script>";           
             $count = $count + 1;
        }
        
        
        if ($referral == $username || $referral == $email)
        {
            $referralerror = "<br> You cannot make yourself a referral.";
            echo "<script type='text/javascript'>
                    $('#referraldiv').addClass('has-error');
                    </script>"; 
             $count = $count + 1;
        }
        //get the member_id of the referral
        if ($referral != null)
        {
          $referral_id = $dataRead->member_referral($mycon, $referral);
            if (!$referral_id)
            {
                $referralfinderror = "Referral could not be found.";
                echo "<script type='text/javascript'>
                    $('#referraldiv').addClass('has-error');
                    </script>"; 
                 $count = $count + 1;
            }  
        }

         if ($count != 0)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **".$count." error was found.<br>".$captchaerror." ".$usernameerror." ".$emailerror." ".$phonenumbererror
                    ." ".$referralerror." ".$referralfinderror.
                "</div>";
            return;
        }
        
        //if all was successful, send a message to the email of the person so as to continue its registrations
        $token = substr(str_shuffle(time()),0,8);
        createCookie("logintoken", $token);
         $sentmessage = "<div class='container'>
                                <p>Hello ".$username.",</p>
                                <p>Enter this token to continue your registration at World Fund Global (WFG): ". $token."  </p>
                                <p><small><em>This message is auto-generated, please do not reply via your email.</em></small></p>
                            </div>";

            $sentmessage = wordwrap($sentmessage,70);
            createCookie("email", $email);
            createCookie("username", $username);
            createCookie("firstname", $firstname);
            createCookie("lastname", $lastname);
            createCookie("phonenumber", $phonenumber);
            createCookie("password", $password);
            createCookie("gender", $gender);
            createCookie("country", $country);
            createCookie("referral", $referral);
            createCookie("captcha", $captcha);
            createCookie("address", $address);

            $message = "A code has been sent to your email ".$email.". Please check to verify your account set up.".$token. ".";
            echo "<div id='successalert'>
                    <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Almost done!</strong> ".$message."
                    </div>
                    </div>
                    <script type='text/javascript'>
                    $('#registerform').hide(500);
                    </script>
                    <form class='form-horizontal m-t-20' action='admin/actionmanager.php' id='emailverifyform'>
                            <div class='form-group'>
                                <div class='col-xs-12 col-md-12 error' id='tokendiv'>
                                    <input class='form-control' name='token' type='text' id='token' placeholder='Input token*''>
                                </div>
                            </div>
                            <div class='form-group text-center m-t-40'>
                            <div class='col-xs-12'>
                                <div class='col-md-8 col-xs-8'>
                                    <button class='btn btn-success btn-block text-uppercase waves-effect waves-light'  id='emailverifybutton' type='button' onclick='submitVerifyForm(this);'>
                                    Verify Account
                                    </button>
                                </div>
                                <div class='col-md-4 col-xs-4'>
                                    <button class='btn btn-danger btn-block text-uppercase waves-effect waves-light' id='emailclearbutton' onclick='resetToken();' type='button'>
                                    Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class='form-group text-center m-t-40'>
                        <div class='col-xs-12'>
                                <div class='col-md-12 col-xs-12'>
                                    <button class='btn btn-primary btn-block text-uppercase waves-effect waves-light' type='button'  id='backtoregisterbutton' onclick='backtoRegistration();'>
                                    << Back to register form
                                    </button>
                                </div>
                            </div>
                            </div>
                        </form>;
                    </script>";
            return;
            

           /**if (sendEmail($email,"Registration Token - Wealth Fund Global", $sentmessage))
           {
                createCookie("email", $email);
                createCookie("username", $username);
                createCookie("firstname", $firstname);
                createCookie("lastname", $lastname);
                createCookie("password", $password);
                createCookie("gender", $gender);
                createCookie("country", $country);
                createCookie("referral", $referral);
                createCookie("phonenumber", $phonenumber);
                createCookie("captcha", $captcha);
                createCookie("address", $address);

                $message = "A code has been sent to your email ".$email.". Please check to activate your account.";
           
                echo "<div id='successalert'>
                    <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Almost done!</strong> ".$message."
                    </div>
                    </div>
                    <script type='text/javascript'>
                    $('#registerform').hide(500);
                    </script>
                    <form class='form-horizontal m-t-20' action='admin/actionmanager.php' id='emailverifyform'>
                            <div class='form-group'>
                                <div class='col-xs-12 col-md-12 error' id='emailverifydiv'>
                                    <input class='form-control' name='emailverify' type='text' id='emailverify' value='".getCookie("email")."' disabled>
                                </div>
                            </div>
                            <div class='form-group'>
                                <div class='col-xs-12 col-md-12 error' id='tokendiv'>
                                    <input class='form-control' name='token' type='text' id='token' placeholder='Input token*''>
                                </div>
                            </div>
                            <div class='form-group text-center m-t-40'>
                            <div class='col-xs-12'>
                                <div class='col-md-8 col-xs-8'>
                                    <button class='btn btn-success btn-block text-uppercase waves-effect waves-light'  id='emailverifybutton' type='button' onclick='submitVerifyForm(this);'>
                                    Verify Account
                                    </button>
                                </div>
                                <div class='col-md-4 col-xs-4'>
                                    <button class='btn btn-danger btn-block text-uppercase waves-effect waves-light' id='emailclearbutton' onclick='resetToken();' type='button'>
                                    Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class='form-group text-center m-t-40'>
                        <div class='col-xs-12'>
                                <div class='col-md-12 col-xs-12'>
                                    <button class='btn btn-primary btn-block text-uppercase waves-effect waves-light' type='button'  id='backtoregisterbutton' onclick='backtoRegistration();'>
                                    << Back to register form
                                    </button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </script>";
                    return;
                }
            else
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                            <i class='fa fa-warning'></i> ** There was an error sending mail to your email, please check your network properly.
                        </div>";
                return;
            } **/
    
            
                      
    }

    function members_token_add()
    {
        $email = getCookie("email");
        $token = $_POST['token'];

        //check if the token is correct
        if ($token != getCookie('logintoken'))
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Invalid token.
                </div>";
            return;
        }

        //get all the cookies saved
        $firstname = getCookie("firstname");
        $lastname = getCookie("lastname");
        $email = getCookie("email");
        $username = getCookie("username");
        $password = getCookie("password");
        $phonenumber = getCookie("phonenumber");
        $gender = getCookie("gender");
        $country = getCookie("country");
        $referral = getCookie("referral");
        $address = getCookie("address");
        $captcha = getCookie("captcha");

        $expiry = date("Y-m-d H:i:s", strtotime("+ 3 days"));

        $password = generatePassword($password);
        
        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();
        $count = 0;
        $captchaerror = '';
        $usernameerror = '';
        $phonenumbererror = '';
        $emailerror = '';
        $referralerror = '';
        $referralfinderror = '';
         //check if the captcha is eqaual to the session captcha
        if ($captcha != $_SESSION['captcha'])
        {
            $captchaerror = "Incorrect Captcha.";
            echo "<script type='text/javascript'>
                    $('#captchadiv').addClass('has-error');
                    </script>";
             $count = $count + 1;
        }
        //check if username exists
        $username_check = $dataRead->member_getbyusername($mycon, $username);
        if ($username_check != false)
        {
            $usernameerror = "<br>Username already exists.";
            echo "<script type='text/javascript'>
                    $('#usernamediv').addClass('has-error');
                    </script>";
             $count = $count + 1;
        }

        //check if email exists
        $email_check = $dataRead->member_getbyemail($mycon,$email);
        if ($email_check != false)
        {
            $emailerror = "<br> Email already exists.";
            echo "<script type='text/javascript'>
                    $('#emaildiv').addClass('has-error');
                    </script>";           
             $count = $count + 1;
        }

        //check if phonenumber exists
        $phonenumber_check = $dataRead->member_getbyphonenumber($mycon,$phonenumber);
        if ($phonenumber_check != false)
        {
            $phonenumbererror = "<br> Phonenumber already exists.";
            echo "<script type='text/javascript'>
                    $('#phonenumberdiv').addClass('has-error');
                    </script>";           
             $count = $count + 1;
        }
        
        if ($referral == $username || $referral == $email)
        {
            $referralerror = "<br> You cannot make yourself a referral.";
            echo "<script type='text/javascript'>
                    $('#referraldiv').addClass('has-error');
                    </script>"; 
             $count = $count + 1;
        }
        //get the member_id of the referral
        if ($referral != null)
        {
          $referral_id = $dataRead->member_referral($mycon, $referral);
            if (!$referral_id)
            {
                $referralfinderror = "Referral could not be found.";
                echo "<script type='text/javascript'>
                    $('#referraldiv').addClass('has-error');
                    </script>"; 
                 $count = $count + 1;
            }  
        }

         if ($count != 0)
        {
            echo "<script type='text/javascript'>
                    $('#emailverifyform').hide();
                    $('#registerform').show(500);
                    </script>
            <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **".$count." error was found.<br>".$captchaerror." ".$usernameerror." ".$emailerror." ".$phonenumbererror." ".$referralerror." ".$referralfinderror.
                "</div>";
            return;
        }


        //start creating the user accounts
        //create the useracccount
        $member_id = '';
        if ($referral == null)
        {
            $member_id = $dataWrite->members_add($mycon,$username,$firstname,$lastname,$password,$email,$phonenumber, $gender,'1',$country,$expiry,$address, $captcha);
        }
        else $member_id = $dataWrite->members_add($mycon,$username,$firstname,$lastname,$password,$email,$phonenumber, $gender,$referral_id['member_id'],$country,$expiry,$address, $captcha);

        if (!$member_id)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Due to security reasons, an error was suspected when saving your information, you will be redirected to the register page to start again.
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='register.php';
            },2000);
                </script>";
            return;
        }

        //first clear off all the cookies before
        setcookie(str_rot13("firstname"),"",time()-3600);
        setcookie(str_rot13("lastname"),"",time()-3600);
        setcookie(str_rot13("email"),"",time()-3600);
        setcookie(str_rot13("username"),"",time()-3600);
        setcookie(str_rot13("password"),"",time()-3600);
        setcookie(str_rot13("gender"),"",time()-3600);
        setcookie(str_rot13("referral"),"",time()-3600);
        setcookie(str_rot13("country"),"",time()-3600);
        setcookie(str_rot13("address"),"",time()-3600);
        setcookie(str_rot13("captcha"),"",time()-3600);

        //generate my sessions cookies
        createCookie("userid",$member_id);
        createCookie("userlogin","YES");
        createCookie("adminlogin", "NO");
        createCookie("username",$username);
        createCookie("email", $email);
        createCookie("fullname",$lastname." ".$firstname);

        //send message to use that account has been verified
          $sentmessage = "<div class='container'>
                                <p>Hello ".$username.",</p>
                                <p>Account verification completed. Ensure you add your bank account details after you are redirected to your 
                                personalized dashboard. Bank account details can be found in the 'Account Section' of the menu items. 
                                Thank you! </p>
                                <p><small><em>This message is auto-generated, please do not reply via your email</em>M/small></p>
                            </div>";
        $sentmessage = wordwrap($sentmessage,70);
        /**if (sendEmail($email, 'Account verified - Wealth Fund Global', $sentmessage))
        {
             echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> We are preparing your dashboard, please wait...
                    </div>
                    <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='dashboard.php';
            },2000);
                </script>";
        }
        else {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **An error occurred while sending message, please check your internet connection properly
                </div>";
        }
        **/
        
        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> We are preparing your dashboard, please wait...
                    </div>
                    <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='dashboard.php';
            },2000);
                </script>";
        
        return;
        
    }

    function member_login()
    {
        $mycon = databaseConnect();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $thedate = date("Y-m-d H:i:s");
        
        $dataread = New DataRead();
        $dataWrite = New DataWrite();
        
        //generate the encoded password
        $password = generatePassword($password);
        $count = 0;
        
        
        //find the member details through th 
        //check whether the email and password exists
        $member_get = $dataread->member_getbyusernamepassword($mycon, $username, $password);
        
        if(!$member_get)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Username or password combination is wrong!
                </div>";
            return;
        }
        if ($member_get['status'] == '0')
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Your account is blocked, please contact support!
                </div>";
            return;
        }
        
        if ($member_get['type'] != 0)
        {
            createCookie("userlogin","NO");
            createCookie("adminlogin", "YES");
        }
        else
        {
            createCookie("userlogin","YES");
            createCookie("adminlogin", "NO");
        }
        
        createCookie("userid",$member_get['member_id']);
        createCookie("id",$member_get['id']);
        createCookie("email", $member_get['email']);
        createCookie("fullname",$member_get['firstname']." ".$member_get['lastname']);
        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Login Success!</strong> Preparing the dashboard..
                    </div>";
        if ($member_get['type'] == '0')
        {
            echo " <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='../index.php';
            },2000);
                </script>";
        }
        else if ($member_get['type'] == '3')
        {
            echo " <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='dashboard.php';
            },2000);
                </script>";
        }
        
        return;
        
    }

    function departments_add()
    {
        $id = $_POST['id']; //getCookie("userid");
        $name = $_POST['name'];
        $info = $_POST['info'];

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        $currentuserid = getCookie("userid");

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }

        //check if the departments exists
        $department_unique = $dataRead->departments_getbycode($mycon, $id);
        if ($department_unique != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the department code already exist. Please refresh your page.!
                </div>
                <script type-'text/javascript'>
                    window.setTimeout(function(){
                document.location.href='department_add.php';
            },2000);
                </script>";
            return;
        }

        //check if the departments exists
        $department_unique = $dataRead->departments_getbyname($mycon, $name);
        if ($department_unique != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the department already exists!
                </div>";
            return;
        }

        //add the departments
        $departments_add = $dataWrite->departments_add($mycon, $name, $id, $info);
        if (!$departments_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }
        //generate new code
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Departments has been added. You can add more...
                    </div>
                    <script type='text/javascript'>
                        $('#department_id').val('DID".$random."');
                        $('#info').val('');
                        $('#name').val('');
                    </script>";
        return;

        
    }


    function staff_add()
    {
        $id = $_POST['id'];
        $age = $_POST['age'];
        $currentsalary = $_POST['currentsalary'];
        $startdate = $_POST['startdate'];
        $position = $_POST['position'];
        $department = $_POST['department']; //getCookie("userid");
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $phonenumber = $_POST['phonenumber'];
        $email = $_POST['email'];
        $info = $_POST['info'];

        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the staff code exists
        $member_code = $dataRead->member_getbycode($mycon, $id);
        if ($member_code != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the staff code already exists! Please refresh
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='staff_add.php';
            },2000);
                </script>";
            return;
        }

        //check if the staff code exists
        $member_phonenumber = $dataRead->member_getbyphonenumber($mycon,$phonenumber);
        if ($member_phonenumber != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, an account is already existing with this phonenumber ".$phonenumber."
                </div>";
            return;
        }

        //check if the email exists
        $member_email = $dataRead->member_getbyemail($mycon, $email);
        if ($member_email != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the staff already exists!
                </div>";
            return;
        }

        $password = generatePassword("000000"); //00000 is the default password
        //add the departments
        $members_add = $dataWrite->members_add($mycon, $id, $firstname, $lastname, $email, $password, $phonenumber, $department, $age, $startdate, $currentsalary, $position, $info,'5', '3'); // 5 signifies active staff and 3 signifies staff level
        if (!$members_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Staff has been added. Staff can login with username <strong>".$email." or ".$id."</strong> and the default password is <strong>'000000'</strong>.
                    </div>
                    <script type='text/javascript'>
                        $('.form-control').val('');
                        $('#staff_id').val('SID".$random."');
                    </script>";
        return;

        
    }


    function customer_add()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $phonenumber = $_POST['phonenumber'];
        $email = $_POST['email'];
        $info = $_POST['info'];

        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the staff code exists
        $member_code = $dataRead->member_getbycode($mycon, $id);
        if ($member_code != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the customer code already exists! Please refresh
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='customer_add.php';
            },2000);
                </script>";
            return;
        }

        //check if the staff code exists
        $member_phonenumber = $dataRead->member_getbyphonenumber($mycon,$phonenumber);
        if ($member_phonenumber != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, an account is already existing with this phonenumber ".$phonenumber."
                </div>";
            return;
        }

        //check if the email exists
        $member_email = $dataRead->member_getbyemail($mycon, $email);
        if ($member_email != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the customer already exists!
                </div>";
            return;
        }

        $password = generatePassword("000000"); //00000 is the default password
        //add the departments
        $members_add = $dataWrite->members_add($mycon, $id, $firstname, $lastname, $email, $password, $phonenumber, "", "", "", "", "", $info,$status, '0'); // 5 signifies active staff and 3 signifies staff level
        if (!$members_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Customer has been added. Customer can login with username <strong>".$email." or ".$id."</strong> and the default password is <strong>'000000'</strong>.
                    </div>
                    <script type='text/javascript'>
                        $('.form-control').val('');
                        $('#customer_id').val('CUID".$random."');
                    </script>";
        return;

        
    }

    function category_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['category_id'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $categoryname_check = $dataRead->category_getbyname($mycon, $name);
        if ($categoryname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the category already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $categoryid_check = $dataRead->category_getbyid($mycon, $id);
        if ($categoryid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the category id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='category_add.php';
            },2000);
                </script>";
            return;
        }

        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $category_add = $dataWrite->category_add($mycon, $id, $name, $info, $currentuserid); 
        if (!$category_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1 && $image['size'] > 0) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/category/{$category_add}.jpg");
        }
        else
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Category added.
                    </div>
               <script type='text/javascript'> 
                    $('#myTable').prepend('<tr>' +
                                                '<td>".$id."</td>' + 
                                                '<td>".$name."</td>' +
                                                '<td>".$fullname."</td>' +
                                                '<td>".$info."</td>' +
                                                '<td><a href=\'category_add.php?id=".$category_add."\'><button class=\'btn btn-info btn-xs\' type=\'button\'>Edit</button></a> <button class=\'btn btn-danger btn-xs\' type=\'button\' onclick=\"if (confirm(\'Are you sure you want to delete this category?\')) location.href=\'category_add.php?delete=".$category_add."\'\">Delete</button></td>' +
                                                '</tr>');
                    $('.form-control').val('');
                    $('#category_id').val('PCID".$random."');
                </script>";
        return;

        
    }


    function carddetails_add()
    {
        $cardnumber = $_POST['cardnumber']; //getCookie("userid");
        $cvc = $_POST['cvc'];
        $nameoncard = $_POST['nameoncard'];
        $expiry = $_POST['expiry'];


    $count = 0;
    $msg = '';
    //validates the inputs
    if ($cardnumber == '')
     {
        $msg += '<br> Card number is required';
        $count += 1;
     }
     if ($cvc == '')
     {
        $msg += '<br> CVC is required';
        $count += 1;
     }
     if ($expiry == '')
     {
        $msg += '<br> Expiry is required';
        $count += 1;
     }
     if ($nameoncard == '')
     {
        $msg += '<br> Name of card is required';
        $count += 1;
     }
     if (strlen($cardnumber) != 16)
     {
        $msg += '<br> Invalid Card Number Digits';
        $count += 1;
     }
     if (strlen($cvc) != 3)
     {
        $msg += '<br> Invalid CVC code';
        $count += 1;
     }
    
     if ($msg != '')
     {
        if ($count == 1)
        {
            
            showAlert("**".$count." error was found, please correct: 
                            <br>".$msg);
        }
        else {
           
            showAlert("**".$count." errors were found, please correct: 
                            <br>".$msg);
        }
        return;
    }

        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            showAlert("Token expired... Please login again.");
            openPage("../login.php?logout=yes");
        }

        $totalamount = 0;
        $totalquantity = 0;
        //get all the product to payeable 
        $productname_check = $dataRead->productcart_getbyidmember($mycon, $currentuserid, '5');
        foreach($productname_check as $row)
        {
            $totalamount = $totalamount + $row['total'];
            $totalquantity = $totalamount + $row['quantity'];
        }

        
        $mycon->beginTransaction();
        
        //add the product to the card details
        $carddetails_add = $dataWrite->carddetails_add($mycon, $currentuserid, $cardnumber, $cvc, $nameoncard, $expiry, $totalquantity, $totalamount);
        if (!$carddetails_add)
        {
            openPage("Sorry, there was an error in your purchase.");
            $mycon->rollBack();
            return;
        }
 

        //change the status of the cart to status 3
        foreach ($productname_check as $row) {
            
            $cartstatus_change = $dataWrite->cartdetails_updatestatus($mycon, $currentuserid, $row['productcart_id'], '3');
            if (!$cartstatus_change)
            {
                showAlert("Sorry, there was an error in your purchase. Please try again.");
                $mycon->rollBack();
                return;
            }  
        }
        
        $mycon->commit();
        showAlert("Purchase was successful. Thank you!");
        openPage("../../index.php");
        return;

        
    }

    function pcategory_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $id = $_POST['category_id'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $categoryname_check = $dataRead->category_getbyname($mycon, $name);
        if ($categoryname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the category already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $categoryid_check = $dataRead->category_getbyid($mycon, $id);
        if ($categoryid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the category id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_add.php';
            },2000);
                </script>";
            return;
        }
        
        //add the departments to the priority
        $category_add = $dataWrite->category_add($mycon, $id, $name, $info, $currentuserid); 
        if (!$category_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Category added.
                    </div>
               <script type='text/javascript'> 
                    $('#appendcategory').append('<label class=\"custom-control custom-checkbox\">' +
                                                '<input type=\"checkbox\" name=\"category[]\" id=\"category\" value=\"".$category_add."\" checked=\"\">' + 
                                                '<span class=\"custom-control-indicator\"></span>' +
                                                '<span class=\"custom-control-description\">".$name."</span>' +
                                                ' </label>'); 
                    $('#cname').val('');
                    $('#desc').val('');
                    $('#category_id').val('PCID".$random."');
                </script>";
        return;

        
    }




    function tag_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['tag_id'];


        $currentuserid = getCookie("userid");
        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $tagname_check = $dataRead->tag_getbyname($mycon, $name);
        if ($tagname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the tag already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $tagid_check = $dataRead->tag_getbyid($mycon, $id);
        if ($tagid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the tag id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='tags.php';
            },2000);
                </script>";
            return;
        }

        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $tag_add = $dataWrite->tag_add($mycon, $id, $name, $info, $currentuserid); 
        if (!$tag_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/tag/{$tag_add}.jpg");
        }
        else if ($image['size'] > 0)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Tag added.
                    </div>
               <script type='text/javascript'> 
                    $('#myTable').prepend('<tr>' +
                                                '<td>".$id."</td>' + 
                                                '<td>".$name."</td>' +
                                                '<td>".$fullname."</td>' +
                                                '<td>".$info."</td>' +
                                                '<td><a href=\'tags.php?id=".$tag_add."\'><button class=\'btn btn-info btn-xs\' type=\'button\'>Edit</button></a> <button class=\'btn btn-danger btn-xs\' type=\'button\' onclick=\"if (confirm(\'Are you sure you want to delete this tag?\')) location.href=\'tags.php?delete=".$tag_add."\'\">Delete</button></td>' +
                                                '</tr>');
                    $('.form-control').val('');
                    $('#tag_id').val('PCID".$random."');
                </script>";
        return;

        
    }

        function ptag_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $id = $_POST['tag_id'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $tagname_check = $dataRead->tag_getbyname($mycon, $name);
        if ($tagname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the tag already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $tagid_check = $dataRead->tag_getbyid($mycon, $id);
        if ($tagid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the tag id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_add.php';
            },2000);
                </script>";
            return;
        }
        
        //add the departments to the priority
        $tag_add = $dataWrite->tag_add($mycon, $id, $name, $info, $currentuserid); 
        if (!$tag_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Tag added.
                    </div>
               <script type='text/javascript'> 
                    $('#appendtag').append('<label class=\"custom-control custom-checkbox\">' +
                                                '<input type=\"checkbox\" name=\"tag[]\" id=\"category\" value=\"".$tag_add."\" checked=\"\">' + 
                                                '<span class=\"custom-control-indicator\"></span>' +
                                                '<span class=\"custom-control-description\">".$name."</span>' +
                                                ' </label>'); 
                    $('#tname').val('');
                    $('#tdesc').val('');
                    $('#tag_id').val('PTID".$random."');
                </script>";
        return;

        
    }



    function category_update()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['category_id'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $categoryupdatename_check = $dataRead->category_getbyupdatename($mycon, $name, $id);
        if ($categoryupdatename_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the category already exists, please try another name!
                </div>";
            return;
        }


        //check if the category name still exists
        $categoryname_check = $dataRead->category_getbyname($mycon, $name);
        if (!$categoryname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the category no longer exist! Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='category_add.php';
            },2000);
                </script>";
            return;
        }
        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $category_add = $dataWrite->category_update($mycon, $id, $name, $info, $currentuserid); 
        if (!$category_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/category/{$categoryname_check['category_id']}.jpg");
        }
        else if ($image['size'] > 0)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Category updated. Refreshing...
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='category_add.php?id=".$categoryname_check['category_id']."';
            },2000);
                </script>";
        return;

        
    }



     function tag_update()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['tag_id'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $tagupdatename_check = $dataRead->tag_getbyupdatename($mycon, $name, $id);
        if ($tagupdatename_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the tag already exists, please try another name!
                </div>";
            return;
        }


        //check if the category name still exists
        $tagname_check = $dataRead->tag_getbyname($mycon, $name);
        if (!$tagname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the tag no longer exist! Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='tags.php';
            },2000);
                </script>";
            return;
        }
        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $tag_update = $dataWrite->tag_update($mycon, $id, $name, $info, $currentuserid); 
        if (!$tag_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1 && $image['size'] > 0) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/tag/{$tagname_check['tag_id']}.jpg");
        }
        else
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Tag updated. Refreshing...
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='tags.php?id=".$tagname_check['tag_id']."';
            },2000);
                </script>";
        return;

        
    }

    function product_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['product_id'];
        $price = $_POST['price'];
        $discountedprice = $_POST['discountedprice'];
        $status =$_POST['status'];
        $category = $_POST['category'];
        $tag = $_POST['tag'];
        $quantity = $_POST['quantity'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $productname_check = $dataRead->product_getbyname($mycon, $name);
        if ($productname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the product already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $productid_check = $dataRead->product_getbyid($mycon, $id);
        if ($productid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the product id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_add.php';
            },2000);
                </script>";
            return;
        }

        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $product_add = $dataWrite->product_add($mycon, $id, $name, $info, $price, $discountedprice, $quantity, $status, $currentuserid); 
        if (!$product_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //save the categories
        foreach ($category as $row => $value) {

            $category_add = $dataWrite->productcategory_add($mycon, $product_add, $value, $currentuserid); 
            if (!$category_add)
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <i class='fa fa-warning'></i> **Unable to perform this operation, please try again! refreshing
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_add.php';
            },3000);
                </script>";
                $mycon->rollBack();
                return;
            }
        }

        foreach ($tag as $row => $value) {

            $tag_add = $dataWrite->producttag_add($mycon, $product_add, $value, $currentuserid); 
            if (!$category_add)
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <i class='fa fa-warning'></i> **Unable to perform this operation, please try again! refreshing
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_add.php';
            },3000);
                </script>";
                $mycon->rollBack();
                return;
            }
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1 && $image['size'] > 0) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/product/{$product_add}.jpg");
        }
        else
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Category added.
                    </div>
               <script type='text/javascript'> 
                    $('.form-control').val('');
                    $('.custom-control-input').attr('checked', false);
                    $('#product_id').val('PID".$random."');
                    $('#category_id').val('PCID".$random."');
                    $('#tag_id').val('PTID".$random."');
                </script>";
        return;

        
    }


    //add the inventory
    function inventory_add()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['inventory_id'];
        $price = $_POST['price'];
        $department = $_POST['department'];
        $quantity = $_POST['quantity'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists
        $inventoryname_check = $dataRead->inventory_getbyname($mycon, $name);
        if ($inventoryname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the inventory already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $inventoryid_check = $dataRead->inventory_getbyid($mycon, $id);
        if ($inventoryid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the inventory id has been used. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='inventory_add.php';
            },2000);
                </script>";
            return;
        }

        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $inventory_add = $dataWrite->inventory_add($mycon, $id, $name, $info, $price, $department, $quantity, $currentuserid); 

        if (!$inventory_add)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }
        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1 && $image['size'] > 0) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/inventory/{$inventory_add}.jpg");
        }
        else
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Please upload an image file!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Inventory Category added.
                    </div>
               <script type='text/javascript'> 
                    $('.form-control').val('');
                    $('.custom-control-input').attr('checked', false);
                    $('#inventory_id').val('PID".$random."');
                </script>";
        return;

        
    }


    function product_update()
    {
        $name = $_POST['name']; //getCookie("userid");
        $info = $_POST['info'];
        $image = $_FILES['image'];
        $id = $_POST['product_id'];
        $price = $_POST['price'];
        $discountedprice = $_POST['discountedprice'];
        $status =$_POST['status'];
        $category = $_POST['category'];
        $tag = $_POST['tag'];
        $quantity = $_POST['quantity'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the category name still exists except the one to be updated
        $productname_check = $dataRead->product_getbyupdatename($mycon, $name, $id);
        if ($productname_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the name of the product already exists, please try another name!
                </div>";
            return;
        }

        //check if the department still exists
        $productid_check = $dataRead->product_getbyupdateid($mycon, $id);
        if (!$productid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the product no longer exists. Refreshing...
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_view.php';
            },2000);
                </script>";
            return;
        }

        //begin mass transaction
        $mycon->beginTransaction();
        
        //add the departments to the priority
        $product_update = $dataWrite->product_update($mycon, $id, $name, $info, $price, $discountedprice, $quantity, $status, $currentuserid); 
        if (!$product_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again! 
                </div>";
            $mycon->rollBack();
            return;
        }

        //delete the category belonging to the product
        $category_delete = $dataWrite->category_deletebyproduct($mycon, $productid_check['product_id']);
        if (!$category_delete)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        //save the categories
        foreach ($category as $row => $value) {

            $category_add = $dataWrite->productcategory_add($mycon, $productid_check['product_id'], $value, $currentuserid); 
            if (!$category_add)
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <i class='fa fa-warning'></i> **Unable to perform this operation, please try again! refreshing
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_view.php';
            },3000);
                </script>";
                $mycon->rollBack();
                return;
            }
        }

        //delete the tag belonging to the product
        $tag_delete = $dataWrite->tag_deletebyproduct($mycon, $productid_check['product_id']);
        if (!$tag_delete)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        foreach ($tag as $row => $value) {

            $tag_add = $dataWrite->producttag_add($mycon, $productid_check['product_id'], $value, $currentuserid); 
            if (!$tag_add)
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <i class='fa fa-warning'></i> **Unable to perform this operation, please try again! refreshing
                    </div>
                    <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_view.php';
            },3000);
                </script>";
                $mycon->rollBack();
                return;
            }
        }

        //upload the image now
         if(strpos(strtoupper($image['type']),"IMAGE") > -1 && $image['size'] > 0) 
        {
                move_uploaded_file($image['tmp_name'],"../uploads/product/{$productid_check['product_id']}.jpg");
        }

        $mycon->commit();
        $fullname = getCookie("fullname");

        //generate a random id number
        $random = substr(str_shuffle(time()),0,4);

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Product Updated.
                    </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='product_edit.php?id=".$productid_check['product_id']."';
            },3000);
                </script>";
        return;

        
    }

    function cart_add()
    {
        $product_id = $_POST['product_id'];
        $stock = $_POST['stock'];
        $quantity = $_POST['quantity']; 
        
        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** You need to login, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='dashboard/login.php?logout=yes';
            },2000);
                </script>";
            return;
        }

        //check if the department still exists
        $productid_check = $dataRead->product_getbyid($mycon, $product_id);
        if (!$productid_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the product has been deleted. Please refresh your code
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='index.php';
            },2000);
                </script>";
            return;
        }

        

        $total = $quantity * $productid_check['discountedprice'];

        //begin mass transaction
        $mycon->beginTransaction();
        
        //find if the product already exist in cart, so one can increase the quantity
        $cartproduct_check = $dataRead->cart_checkbyidproduct($mycon, $product_id, $currentuserid);
        if ($cartproduct_check)
        {
            $total = $total + $cartproduct_check['total'];

            $quantity = $quantity + $cartproduct_check['quantity'];
            //add the departments to the priority
            $cart_update = $dataWrite->cart_update($mycon, $product_id, $currentuserid, $quantity, $total); 
            if (!$cart_update)
            {
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                    </div>";
                $mycon->rollBack();
                return;
            }
        }
        else
        {
            //add the departments to the priority
                $cart_add = $dataWrite->cart_add($mycon, $product_id, $currentuserid, $quantity, $total); 
                if (!$cart_add)
                {
                    echo "<div class='alert alert-danger alert-dismissable'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                            <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                        </div>";
                    $mycon->rollBack();
                    return;
                }
        }
        

        //decrease the stock in the product databases
        $stockleft = $productid_check['stock'] - $quantity;

        $product_reduce = $dataWrite->product_stockreduce($mycon, $product_id, $stockleft); 
        if (!$product_reduce)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            $mycon->rollBack();
            return;
        }

        $mycon->commit();

        //get the product by 
        $product_get = $dataRead->cart_checkbyidproduct($mycon, $product_id, $currentuserid);
        $fullname = getCookie("fullname");

        echo "<script type='text/javascript'>
                $('#cartappend').prepend('<li>' +
                                            '<div class=\'cart-img\'><img src=\'dashboard/uploads/product/".$product_id.".jpg\' /></div>' +
                                            '<div class=\'cart-content text-center\'>' +
                                            '<h5>".$product_get['name']." X ".$product_get['quantity']." </h5><small>&#8358;".number_format($product_get['discountedprice'], 0,'.', ',')."</small></div>' +
                                        '</li>');
                
                $('#nocart').remove();
                 swal({   
                    title: 'Product added to Cart',   
                    text: 'This product has been added to cart',   
                    timer: 2000,  
                    type: 'success', 
                    showConfirmButton: true,
                    showCancelButton: true,   
                    confirmButtonColor: '#DD6B55',   
                    confirmButtonText: 'Go to Check Out',   
                    cancelButtonText: 'Continue Shopping',   
                    closeOnConfirm: false,   
                    closeOnCancel: true  
                    }, function(isConfirm){   
            if (isConfirm) {     
                document.location.href='product_checkout.php'; 
            } else {     
                return;   
            } 
        });
               </script>";
        return;

        
    }

    function priority_update()
    {
        $department = $_POST['department']; //getCookie("userid");
        $instruction = $_POST['instruction'];
        $priority_id = $_POST['priority_id'];
        $deadline = $_POST['deadline'];


        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the department still exists
        $department_check = $dataRead->departments_getbyiddapartments($mycon, $department);
        if (!$department_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the departments no longer exist. Refreshing...!
                </div>
                <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='clearance_priority.php';
            },2000);
                </script>";
            return;
        }

        //check if the department priority is saved
        $priority_check = $dataRead->priority_getbyidothers($mycon, $department, $priority_id);
        if ($priority_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Sorry, the clearance department already exists!
                </div>";
            return;
        }

        if ($deadline == '')
        {
            $deadline = $priority_check['deadline'];
        }

        
        //add the departments to the priority
        $priority_update = $dataWrite->priority_update($mycon, $priority_id, $department, $instruction, $deadline); 
        if (!$priority_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> clearance department updated. Refreshing...
                    </div>
                    <script type='text/javascript'>
                        $('#department').val('');
                        $('#instruction').val('');
                        window.setTimeout(function(){
                document.location.href='clearance_priority.php';
            },2000);
                    </script>";
        return;

        
    }


    function departments_update()
    {
        $name = $_POST['name'];
        $id = $_POST['id'];
        $info = $_POST['info'];

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        $currentuserid = getCookie("userid");
        

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the department exists
        $departmentdetails = $dataRead->departments_getbycode($mycon, $id);
        if (!$departmentdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Department not found. Refreshing...
                </div>
                <script type='text/javascript'>
                        window.setTimeout(function(){
                document.location.href='department_view.php';
            },2000);
                    </script>";
            return;
        }

        //add the departments
        $departments_update = $dataWrite->departments_update($mycon, $id, $name, $info); //status 5 shows the head of the department
        if (!$departments_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Departments has been updated.
                    </div>";
        return;

        
    }

    function staff_update()
    {
        $id = $_POST['id'];
        $age = $_POST['age'];
        $currentsalary = $_POST['currentsalary'];
        $startdate = $_POST['startdate'];
        $position = $_POST['position'];
        $department = $_POST['department']; //getCookie("userid");
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $phonenumber = $_POST['phonenumber'];
        $email = $_POST['email'];
        $info = $_POST['info'];
        $password = $_POST['password'];

        $currentuserid = '4'; //getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by currentuserid
        $staffdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$staffdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** You have been logged out. Please login again...
                </div>
                <script type='text/javascript'>
                        window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                    </script>";
            return;
        }


        //cehck if the email address exists
        $email_check = $dataRead->member_getbyemail($mycon, $email);
        if (!$email_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Staff no longer exists!
                </div>";
            return;
        }

        //cehck if the email address exists
        $phonenumber_check = $dataRead->member_getbyphonenumber($mycon, $phonenumber, $email_check['member_id']);
        if (!$phonenumber_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Phone number already been used by another staff!
                </div>";
            return;
        }
        

        //check if the password is empty or not
        if ($password == '')
        {
            $password = $email_check['password'];
        }
        else 
        {
            $password = generatePassword($password);
        }
        //add the departments
        $staff_update = $dataWrite->members_update($mycon, $email_check['member_id'], $firstname, $lastname, $password, $phonenumber, $department, $age, $startdate, $currentsalary, $position, $info); //status 5 shows the head of the department
        if (!$staff_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Staff updated! Refreshing...
                    </div>
                    <script type='text/javascript'>
                        window.setTimeout(function(){
                            document.location.href='staff_view.php';
                        },3000);
                    </script>";
        return;

        
    }

    //add new students
    function students_add()
    {
        $username = $_POST['username']; //getCookie("userid");
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $bio = $_POST['bio'];
        $status = $_POST['status'];

        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Token expired, please login again...
                </div>
                 <script type='text/javascript'>
                    window.setTimeout(function(){
                document.location.href='login.php?logout=yes';
            },2000);
                </script>";
            return;
        }


        //check if the username exists
        $member_email = $dataRead->member_getbyusername($mycon, $username);
        if ($member_email != false)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **sorry, the matric number already exists!
                </div>";
            return;
        }

        


        $password = generatePassword("000000"); //00000 is the default password
        $mycon->beginTransaction();
        //add the departments
        $members_add = $dataWrite->members_add($mycon, $firstname, $lastname, $password, $username, '0', $bio,'5', '0'); // 5 signifies active staff and 3 signifies staff level
        if (!$members_add)
        {
            $mycon->rollBack();
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        //get the priority lists
        $priority_get = $dataRead->priority_getone($mycon);
        if (!$priority_get)
        {
            $mycon->rollBack();
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }
        //begin the clearance processing
        $clearance_add = $dataWrite->clearance_add($mycon, $members_add, $priority_get['department_id'], $priority_get['priority_id'], '', '5');
        if (!$clearance_add)
        {
            $mycon->rollBack();
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        $mycon->commit();

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Student has been added. Username is ".$username." and default password is '000000'.
                    </div>
                    <script type='text/javascript'>
                        $('#firstname').val('');
                        $('#lastname').val('');
                        $('#status').val('');
                        $('#bio').val('');
                        $('#username').val('');
                    </script>";
        return;

        
    }

    function students_update()
    {
        $status = $_POST['status']; //getCookie("userid");
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $member_id = $_POST['member_id'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $bio = $_POST['bio'];

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $studentdetails = $dataRead->member_getbyid($mycon, $member_id);
        if (!$studentdetails)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> ** Student not found, please refresh your page
                </div>";
            return;
        }


        //cehck if the usernames exists
        $email_check = $dataRead->member_getbyusername($mycon, $username);
        if (!$email_check)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Student no longer exists!
                </div>";
            return;
        }
        

        //check if the password is empty or not
        if ($password == '')
        {
            $password = $email_check['password'];
        }
        else 
        {
            $password = generatePassword($password);
        }
        //add the departments
        $student_update = $dataWrite->members_update($mycon, $member_id, $firstname, $lastname,$password, $bio, $status); //status 5 shows the head of the department
        if (!$student_update)
        {
            echo "<div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <i class='fa fa-warning'></i> **Unable to perform this operation, please try again!
                </div>";
            return;
        }

        echo "<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <strong><i class='fa fa-smile-o'></i> Success!</strong> Student updated! Refreshing...
                    </div>
                    <script type='text/javascript'>
                        window.setTimeout(function(){
                            document.location.href='students_view.php';
                        },2000);
                    </script>";
        return;

        
    }

    //add new students
    function clearance_add()
    {
        $addinfo = $_POST['addinfo']; //getCookie("userid");
        $document = $_FILES['document'];
        $priority_id = $_POST['priority_id'];

        $currentuserid = getCookie("userid");

        $dataRead = New DataRead();
        $dataWrite = New DataWrite();
        $mycon = databaseConnect();

        //check if user is signed in by getting the user by member id
        $memberdetails = $dataRead->member_getbyid($mycon, $currentuserid);
        if (!$memberdetails)
        {
            showAlert("Token expired... please login again");
            openPage("../login.php?logout=yes");
            return;
        }

        if(($document['size']) < 1) 
        {
                showAlert("No document attached. Please attached at least the document required.");
                return;
        }


        //check if the username exists
        $clearance_status_get = $dataRead->clearance_status_get($mycon, $currentuserid, $priority_id, '5');
        if (!$clearance_status_get)
        {
            showAlert("The clearance status for this department is already submitted, waiting to be reviewed. Please refresh your page!");
            openPage("../clearance_proceed.php");
            return;
        }

        $mycon->beginTransaction();

        //update the clearance info supplied
        $clearance_update = $dataWrite->clearance_status_update($mycon, $clearance_status_get['clearance_status_id'], $addinfo, '3');
        if (!$clearance_update)
        {
            $mycon->rollBack();
            showAlert("Unable to perform this request. Please try again!1");
            return;
        }

        //move the document to one of the folder called uploads
        if(strpos(strtoupper($document['type']),"IMAGE") > -1 || strpos(strtoupper($document['type']),"PDF") > -1) 
        {
                move_uploaded_file($document['tmp_name'],"../uploads/{$document['name']}");
        }
        else
        {
            $mycon->rollBack();
            showAlert("Only image and pdf documents is allowed. Please upload appropraitely2");
            return;
        }
        

        //get the next phase of clearance
        //get the priority lists
        $priority_get = $dataRead->priority_getnext($mycon, $clearance_status_get['priority_id']);
        if (!$priority_get)
        {
            $mycon->commit();
            showAlert("All clearance documents have been submitted. Please wait patiently for approval. Thanks");
            openPage("../clearance_proceed.php");
            return;
        }
        //begin the clearance processing
        $clearance_add = $dataWrite->clearance_add($mycon, $clearance_status_get['member_id'], $priority_get['department_id'], $priority_get['priority_id'], '', '5');
        if (!$clearance_add)
        {
            $mycon->rollBack();
            showAlert("Unable to perform this operation. Please try again!");
            return;
        }

        $mycon->commit();

        showAlert("This clearance submitted. Please proceed to the next phase!");
        openPage("../clearance_proceed.php");
    }

}

?>