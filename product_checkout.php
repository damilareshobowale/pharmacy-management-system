<?php
include_once("dashboard/admin/config.php");
include_once("dashboard/admin/inc_dbfunctions.php");

//generate a random id number
$random = substr(str_shuffle(time()),0,4);

$currentuserid = getCookie("userid");
$mycon = databaseConnect();

$dataRead = New DataRead();

//get the details of the member
$memberdetails = $dataRead->member_getbyid($mycon,$currentuserid);

//get
$productcartdetails = $dataRead->productcart_getall($mycon, $currentuserid);


if (isset($_GET['delete']) && $_GET['delete'] != '') deleteCart($_GET['delete']);


function deleteCart($delete)
{
    $dataRead = New DataRead();
    $dataWrite = New DataWrite();
    $mycon = databaseConnect();


    //find if the cart id exists
    $category_check = $dataRead->cart_getbyidproductcart($mycon,$delete);
    if (!$category_check)
    {
        showAlert("Sorry, this cart no longer exists.");
        openPage("product_checkout.php");
    }


    //delete the category and then refresh the page
    $category_delete = $dataWrite->productcart_delete($mycon, $delete);
    if (!$category_delete)
    {
        showAlert("This product in the cart could not be deleted. Please try again");
        openPage("product_checkout.php");
    }

    showAlert("Product in the cart has been deleted. Please press OK to refresh");
    openPage("product_checkout.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="dashboard/plugins/images/favicon.png">
    <title><?php echo pageTitle(); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="dashboard/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dashboard/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="dashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link src="dashboard/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- color CSS -->
    <link src="dashboard/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div id="wrapper">
        <?php include_once("inc_header.php");  ?>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Checkout Page</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php">All Products</a></li>
                            <li class="active">Check Out</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                  <!-- row -->
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h1 class="m-t-0"><i class="fa fa-cc-visa text-info"></i></h1>
                            <h2>**** **** **** 2150</h2> <span class="pull-right">Expiry date: 10/18</span> <span class="font-500">Sample Example</span> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h1 class="m-t-0"><i class="fa fa-cc-mastercard text-danger"></i></h1>
                            <h2>**** **** **** 2150</h2> <span class="pull-right">Expiry date: 10/18</span> <span class="font-500">Sample Example</span> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h1 class="m-t-0"><i class="fa fa-cc-discover text-success"></i></h1>
                            <h2>**** **** **** 2150</h2> <span class="pull-right">Expiry date: 10/18</span> <span class="font-500">Sample Example</span> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h1 class="m-t-0"><i class="fa fa-cc-amex text-warning"></i></h1>
                            <h2>**** **** **** 2150</h2> <span class="pull-right">Expiry date: 10/18</span> <span class="font-500">Sample Example</span> </div>
                    </div>
                </div>
                <!--/.row -->
                <!-- /row -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Product Summary</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Photo</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        $totalamount = 0;
                                        if ($productcartdetails != null)
                                        {
                                        foreach ($productcartdetails as $row) {
                                            $totalamount = $totalamount + $row['total'];
                                            $id = $row['productcart_id'];
                                        ?>
                                        <tr>
                                            <td><a href="product_view.php?product=<?php echo $row['product_id'] ?>"><img src="dashboard/uploads/product/<?php echo $row['product_id'] ?>.jpg" alt="iMac" width="80"></td>
                                            <td><?php echo $row['name'] ?></td>
                                            <td><?php echo $row['quantity'] ?></td>
                                            <td class="font-500">&#8358;<?php echo number_format($row['total'], 0, '.', ',') ?></td>
                                            <td><button class="btn btn-danger btn-xs" type="button" onclick="if (confirm('Are you sure you want to remove this product from cart?')) location.href='product_checkout.php?delete=<?php echo $id ?>'">Remove</button></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                        <tr>
                                            <td colspan="3" class="font-500" align="right">Total Amount</td>
                                            <td class="font-500">&#8358;<?php echo number_format($totalamount, 0, '.', ','); ?></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                        <tr>
                                            <td colspan="3" align="right">Cart is Empty</td>
                                            <td><a href="index.php"><i class="fa fa-backward m-r-5"></i> Go to Products </a></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <?php
                                if ($productcartdetails != null)
                                {
                            ?>
                            <h3 class="box-title">Choose payment Option</h3>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active nav-item"><a href="#iprofile" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Debit Card</span></a></li>
                                <li role="presentation" class="nav-item"><a href="#ihome" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Paypal</span></a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="ihome"> You can pay your money through paypal, for more info <a href="">click here</a>
                                    <br>
                                    <br>
                                    <button class="btn btn-info"><i class="fa fa-cc-paypal"></i> Pay with Paypal</button>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="iprofile">
                                    <div class="col-md-7 col-sm-5">
                                        <iframe id="result" width="100%" height="150px" name="result"></iframe>
                                        <form action="dashboard/admin/actionmanager.php" id="carddetails_add" role="form" method="post" target="result">
                                            <div class="form-group">
                                                <label for="cardnumber">CARD NUMBER</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                                    <input type="text" class="form-control" id="cardnumber" name="cardnumber" placeholder="Card Number"> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-7 col-md-7">
                                                    <div class="form-group">
                                                        <label>EXPIRATION DATE</label>
                                                        <input type="month" class="form-control" id="expiry" name="expiry" placeholder="MM / YY"> </div>
                                                </div>
                                                <div class="col-xs-5 col-md-5 pull-right">
                                                    <div class="form-group">
                                                        <label>CV CODE</label>
                                                        <input type="text" class="form-control" name="cvc" placeholder="CVC" id="cvc"> </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>NAME OF CARD</label>
                                                        <input type="text" class="form-control" name="nameoncard" placeholder="NAME AND SURNAME" id="nameoncard"> </div>
                                                </div>
                                            </div>
                                            <input type="hidden" class="form-control" name="command" value="carddetails_add"> 
                                            <button type="submit" id="carddetails_addbutton" class="btn btn-info">Make Payment</button>
                                        </form>
                                    </div>
                                    <div class="col-md-4 col-sm-5 pull-right">
                                        <h3 class="box-title m-t-10">General Info</h3>
                                        <h2><i class="fa fa-cc-visa text-info"></i> <i class="fa fa-cc-mastercard text-danger"></i> <i class="fa fa-cc-discover text-success"></i> <i class="fa fa-cc-amex text-warning"></i></h2>
                                        <p>Debit cards acceptable includes: VISA, MASTERCARD, DISCOVER AND AMERICA EXPRESS</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"><?php echo date("Y"); ?> &copy; Web Based Pharmacy Management System </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="dashboard/bootstrap/dist/js/tether.min.js"></script>
    <script src="dashboard/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="dashboard/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="dashboard/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="dashboard/js/waves.js"></script>
    <!--Counter js -->
    <script src="dashboard/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="dashboard/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="dashboard/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="dashboard/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dashboard/js/custom.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="dashboard/js/dashboard1.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="dashboard/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="dashboard/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script type="text/javascript" src="dashboard/js/ajax.js"></script>
</body>

</html>
